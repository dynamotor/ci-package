<?php

$lang['member_login'] = 'Sign In';
$lang['member_logout'] = 'Sign Out';

$lang['member_already'] = 'Member Already?';

$lang['member_signn'] = 'Sign In';
$lang['member_signout'] = 'Sign Out';

$lang['member_profile'] = 'Profile';
$lang['member_register'] = 'Join Us';
$lang['member_register_now'] = 'Join Us';
$lang['member_register_button'] = 'Register now';
$lang['member_agree'] = 'Agree';

$lang['member_change_password'] = 'Change Password';
$lang['member_reset_password'] = 'Reset Password';


$lang['profile_loginid'] = 'Login ID';

$lang['profile_username'] = 'User Name';
$lang['profile_username_description'] = 'Must be 6-24 characters long. First character must be in letter. The other could be formed with letters, numbers.';
$lang['profile_username_error_empty'] = 'Please enter your username.';
$lang['profile_username_error_used'] = 'This user name has been reserved. If you are the user name owner, please try to login or reset your password.';
$lang['profile_username_error_invalid'] = 'Invalid format';


$lang['profile_last_name'] = 'Last Name';

$lang['profile_first_name'] = 'First Name';

$lang['profile_phone'] = 'Phone';

$lang['profile_email'] = 'Email';
$lang['profile_email_description'] = 'A confirmation will be sent to your email';
$lang['profile_email_error_empty'] = 'Please enter your email address.';
$lang['profile_email_error_used'] = 'This email address has been reserved. If you are the email address owner, please try to login or reset your password.';
$lang['profile_email_error_invalid'] = 'Invalid format';

$lang['profile_email_confirm'] = 'Email Confirm';
$lang['profile_email_confirm_description'] = 'Please enter your email address again';
$lang['profile_email_confirm_error_notmatch'] = 'Confirm email address does not match.';


$lang['profile_password'] = 'Password';
$lang['profile_password_new'] = 'New Password';
$lang['profile_password_old'] = 'Old Password';
$lang['profile_password_old_incorrect'] = 'Entered password is not matched in our database.';
$lang['profile_password_current'] = 'Current Password';
$lang['profile_password_current_description'] = 'Enter your existing password.';
$lang['profile_password_change'] = 'Change Password';
$lang['profile_password_change_success'] = 'Change Password Successfully';
$lang['profile_password_change_fail'] = 'Change Password Fail';
$lang['profile_password_description'] = 'Must be 6-24 characters long (letters &amp; numbers only)';
$lang['profile_password_error_invalid'] = 'Invalid Format';
$lang['profile_password_error_empty'] = 'Please enter your login password.';
$lang['profile_password_error_same'] = 'New password cannot same as your existing password.';

$lang['profile_password_confirm'] = 'Password Confirm';
$lang['profile_password_confirm_description'] = 'Please enter your password again';
$lang['profile_password_confirm_error_notmatch'] = 'Confirm password does not match.';

$lang['profile_date_of_birth'] = 'Date of Birth.';
$lang['profile_date_of_birth_month'] = 'Month';
$lang['profile_date_of_birth_year'] = 'Year';
$lang['profile_date_of_birth_notprovide'] = 'Not Provide';

$lang['securecode'] = 'Secure Code';
$lang['securecode_description'] = 'Please enter exactly the letters and numbers in the box below.';
$lang['securecode_error_empty'] = 'Please enter Secure Code.';
$lang['securecode_error_incorrect'] = 'Secure Code is incorrect, please try again.';
$lang['securecode_refresh_link'] = 'Refresh Secure Code?';

$lang['profile_agree_error_empty'] = 'You must agree in order to continue registration.';

$lang['profile_privacy_link'] = 'Privacy Policy';
$lang['profile_privacy_agress_link'] = 'I agress the %sPrivacy Policy%s';

$lang['profile_agree_option_yes'] = 'Yes, I Agree';
$lang['profile_agree_option_no'] = ' No, I disagree.';
$lang['profile_agree_description'] = 'If you disagree the statment, you cannot continue the registration process.';

$lang['profile_subscribe'] = 'I would like to receive any promotion letter or newsletter from UNKNOWN';

$lang['profile_display_name'] = 'Display Name';
$lang['profile_full_name'] = 'Full Name';

$lang['profile_company'] = 'Company';
$lang['profile_building'] = 'Building';
$lang['profile_street'] = ' Street';
$lang['profile_city'] = 'City';
$lang['profile_distrcit'] = 'Distrcit';
$lang['profile_postalcode'] = 'Postal Code';
$lang['profile_country'] = 'Country';

$lang['member_register_form_remark'] = 'Please complete the registration form';
$lang['member_register_complete'] = '';
$lang['member_register_submit'] = 'Register Now';


$lang['member_change_password_description'] = 'You can change your password by using your currently valid password. Please fill-in all the blank and submit form for changing your password.';

$lang['profile_current_password'] = 'Current Password';
$lang['profile_current_password_error_empty'] = 'Please enter your valid password';
$lang['profile_current_password_error_incorrect'] = 'Your old password is incorrect';

$lang['profile_new_password'] = 'New Password';
$lang['profile_new_password_error_empty'] = 'Please enter your new password';

$lang['profile_new_password_confirm'] = 'Re-type New Password';

$lang['member_signin_required'] = 'You must sign-in first before continuing the page';

$lang['member_signin_description'] = 'Please sign in before continuing';

$lang['member_signin_welcome'] = 'Welcome back';
$lang['member_have_problem'] = 'Have problems?';
$lang['member_not_registered'] = 'Not registered?';

$lang['member_signin_error_unknown'] = 'Sorry, but we could not handle your request. Please try again later.';
$lang['member_signin_error_notfound'] = 'Sorry, but we cannot found your account';
$lang['member_signin_error_notverify'] = 'Account does not verified';

$lang['member_verify_link_message'] = 'Click here to resend the verify letter';
$lang['member_signin_password_error_incorrect'] = 'Password is incorrect. Please enter again.';

$lang['member_verify_resend_success'] = 'We had send you a mail for verify your email address. Please check your mailbox.';
$lang['member_verify_resend'] = 'Re-send Verify Mail';

$lang['member_verify_resend_description'] = 'Please enter your registered email address to verify your account';

$lang['profile_email_error_not_found'] = 'Email does not registered yet.';
$lang['profile_email_error_verified'] = 'Email had been verified';

$lang['member_profile_welcome'] = 'Yo! ';
$lang['member_profile_logged_as'] = 'You are logged as %s';


$lang['member_profile_edit'] = 'Edit Profile';
$lang['order_history'] = 'Order History';
$lang['member_profile_other_option'] ='Other options';

$lang['membership_view'] = 'My Membership';
