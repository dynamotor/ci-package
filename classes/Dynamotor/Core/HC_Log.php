<?php
namespace Dynamotor\Core;
use \CI_Log;

class HC_Log extends CI_Log 
{
    public function is_allowed($level = 'debug') {
        if ($this->_enabled === FALSE)
		{
			return FALSE;
		}

		$level = strtoupper($level);

		if (( ! isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold))
			&& ! isset($this->_threshold_array[$this->_levels[$level]]))
		{
			return FALSE;
        }
        
        return TRUE;
    }
}