<?php

/**
* "HC_Model" Model Class for CodeIgniter
* @author      Leman Kwok
* @copyright   Copyright (c) 2013, LMSWork.
* @license     http://codeigniter.com/user_guide/license.html
* @link        http://lmswork.com
* @version     Version 1.2
*
*/

// ------------------------------------------------------------------------

namespace Dynamotor\Core;
use \CI_Model;

use \Dynamotor\Core\HC_Exception;

use \Dynamotor\Data\FetchedResult;
use \Dynamotor\Data\Field;
use \Dynamotor\Data\Entity;

class HC_Model extends CI_Model 
{
	public function __construct($config = NULL) {
		parent::__construct();

		$this->initialize($config);
	}

	/**
	 * The list of existing connection.
	 */
	static $connections = [];

	var $db_group_name = NULL;

	var $auto_increment = true;// true = counted the record identifier by database AUTO_INCREMENT if supported
	var $use_guid       = false;// true = use guid() to create 36-char unique id by mac address, ip address

	var $_log_query = false;

	var $table          =  NULL;//'table_name' table name without prefix
	var $default_values = array();
	var $fields = NULL;
	var $fields_alias = NULL;
	/*
	var $fields         = array('id', 'create_date', 'create_by', 'create_by_id', 'modify_date', 'modify_by', 'modify_by_id');
	var $fields_alias   = array('id' => 'id', 'create_date' => NULL, 'modify_date' => NULL);// fields
	//*/
	var $pk_field       = 'id';
	var $mapping_field  = array('id', 'sys_name', 'slug', 'shortcode');
	var $table_indexes  = array();

	var $fields_details = NULL;
	var $auto_install   = TRUE;

	var $no_space_fields = array('sys_name','slug');
	

	var $create_date_field = 'create_date';
	var $modify_date_field = 'modify_date';
	var $create_by_field = 'create_by';
	var $modify_by_field = 'modify_by';
	var $create_by_id_field = 'create_by_id';
	var $modify_by_id_field = 'modify_by_id';

	var $serializable_fields = ['parameters'];

	/*
	$fields_details = array(
	'blog_id' => array(
	'type' => 'INT',
	'constraint' => 5,
	'unsigned' => TRUE,
	'auto_increment' => TRUE
	),
	'blog_title' => array(
	'type' => 'VARCHAR',
	'constraint' => '100',
	),
	'blog_author' => array(
	'type' =>'VARCHAR',
	'constraint' => '100',
	'default' => 'King of Town',
	),
	'blog_description' => array(
	'type' => 'TEXT',
	'null' => TRUE,
	),
	);
	 */
	
	protected $_is_init = false;
	protected $_is_installed = false;

	protected $_config = null;
	
	public function initialize($config= NULL){

		$this->_config = $config;

		$this->init_fields();

		$db_instance_name = $this->get_db_instance_name();
		$this->_is_init =  isset(self::$connections[ $db_instance_name ]);
	}
	
	protected function initDbConn()
	{
		$db_instance_name = $this->get_db_instance_name();

		if(!isset(self::$connections[ $db_instance_name ])){
			// log_message('debug',__METHOD__.'['.$this->table.'] existing instances: '.print_r(array_keys(self::$connections), true));
			// log_message('debug',__METHOD__.'['.$this->table.'] tryiing to connect db [instance='.$db_instance_name.']');

			$db_instance = $this->load->database($db_instance_name, TRUE);

			if(empty($db_instance)) {
				throw new \Exception('Database instance did not create.');
			}

			self::$connections[ $db_instance_name ] = $db_instance;
			$this->_is_init = true;
			
		}

		$this->perform_auto_install();
	}

	public function init_fields(){

		if(empty($this->fields) && !empty($this->fields_details)){
			$this->fields = array_keys($this->fields_details);
		}
	}

	public function perform_auto_install(){

		if ($this->auto_install && !$this->_is_installed) {
			$this->_is_installed = true;
			$this->install();

		}
	}

	public function get_db_instance_name()
	{
		return $this->getDbInstanceName();
	}

	public function getDbInstanceName()
	{
		if(empty($this->db_group_name))
			return 'default';

		return $this->db_group_name;
	}

	public function get_db(){
		return $this->getDb();
	}

	public function getDb(){
		$this->initDbConn();

		$db_instance_name = $this->get_db_instance_name();

		if(isset(self::$connections[ $db_instance_name ])){
			return self::$connections[ $db_instance_name ];
		}
		return null;
	}

	public function has_field($field_name = ''){
		return in_array($field_name, $this->fields);
	}


	public function get_dbforge()
	{
		return $this->getDbforge();
	}

	public function getDbforge()
	{
		$db = $this->getDb();
		if($db == null) return null;

		return $this->load->dbforge($db, true);
	}

	/***************************************************************************************************/
// Public functions

	public function install() {

		if(empty($this->fields) && !empty($this->fields_details)){
			$this->fields = array_keys($this->fields_details);
		}
			
		if (!empty($this->table) && $this->fields_details != NULL) {
			return $this->install_table($this->table, $this->fields, $this->fields_details, $this->table_indexes);
		}
	}
		
	public function base_table(){
		return $this->table;
	}

	public function table(){
		return $this->getDb()->dbprefix($this->table);
	}

	public function table_exist($table = false) {
		if (!$table) {
			$table = $this->table;
		}

		return $this->getDb()->table_exists($table);
	}

	protected function install_table($table, $table_fields, $table_field_details, $table_indexes = NULL) {

		$db = $this->getDb();
		if ($db && !$db->table_exists($table)) {
			$forge = $this->get_dbforge();

			$fulltext_fields = [];
			$fulltext_ngram = false;

			$_fields = array();
			foreach ($table_fields as $idx => $field_name) {
				if (!isset($table_field_details[$field_name])) {
					$class_name = get_class($this);
					log_message('error',__METHOD__.'['.$this->table.'] cannot find details to install '.print_r($table_fields, true));

					throw new \Exception('Cannot install model since missing field\'s detail of "' . $field_name . '" in Model Class: ' . $class_name);
				}
				$field_detail = $table_field_details[$field_name];

				if (isset($field_detail['pk']) && $field_detail['pk'] == true) {
					$forge->add_key($field_name, true);
				} elseif ($table == $this->table && is_array($this->pk_field) && in_array($field_name, $this->pk_field)) {
					$forge->add_key($field_name, true);
				} elseif ($table == $this->table && $this->pk_field == $field_name) {
					$forge->add_key($field_name, true);
				}

				// FULLTEXT SEARCH
				if(!empty($field_detail['fulltext']) && $field_detail['fulltext']){
					if(!in_array($field_name, $fulltext_fields)){
						$fulltext_fields[] = $field_name;
						if(isset($field_detail['fulltext']['ngram'])){
							$fulltext_ngram = true;
						}
					}
				}

				$_fields[$field_name] = $field_detail;
			}
			$forge->add_field($_fields);

			if(!empty($fulltext_fields)){

				// For MySQL Version 5.7.6, it comes with plugin 'ngram' for chinese full-text search
				if($fulltext_ngram){
					$forge->add_field("FULLTEXT idx_fulltext (".implode(',', $fulltext_fields).") with parser ngram");
				}else{
					$forge->add_field("FULLTEXT idx_fulltext (".implode(',', $fulltext_fields).")");
				}
			}

			if (!empty($table_indexes)) {
				foreach ($table_indexes as $idx => $index_group) {
					$forge->add_key($index_group);
				}
			}


			$forge->create_table($table);
		}
	}

	public function uninstall() {

		$db = $this->getDb();
		if ($db ) {
			$forge = $this->load->dbforge($db, true);

			if ($this->auto_install && !empty($this->table) && !empty($this->table) && !empty($this->fields_details)) {
				$forge->drop_table($this->table);
			}
		}
	}

	public function new_default_values() {
		return $this->getDefaultValues();
	}
	public function getDefaultValues() {

		$def_vals = new Entity();
		foreach ($this->fields as $idx => $field_name) {
			if (isset($this->default_values[$field_name])) {
				$def_vals[$field_name] = $this->default_values[$field_name];
			}elseif(isset($this->fields_details[$field_name]['default_value'])){
				$def_vals[$field_name] = $this->fields_details[$field_name]['default_value'];
			}elseif(isset($this->fields_details[$field_name]['default'])){
				$def_vals[$field_name] = $this->fields_details[$field_name]['default'];
			}elseif(isset($this->fields_details[$field_name]['null']) && $this->fields_details[$field_name]['null']){
				$def_vals[$field_name] = NULL;
			}else{
				$def_vals[$field_name] = '';
			}
		}
		return $def_vals;
	}

	public function validate($data, $options = false) {
		$success = true;
		$fields  = array();
		$issues  = array();
		return compact('success', 'fields', 'issues');
	}

	/**
	 * Save a record
	 *
	 * @param      <type>   $data     The data
	 * @param      boolean  $conds    The conds
	 * @param      boolean  $options  The options
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function save($data, $conds = false, $options = false) {
		$this->initDbConn();

		if(is_object($data) && $data instanceof \Dynamotor\Data\Entity){
			$data = $data->toArray();
		}

		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' .':'.__LINE__. ', '."\r\nargs=" . print_r(compact('data','conds','options'), true));
		}


		$id_field = $this->_field($this->pk_field, false);

		$table     = $this->_table($options, $this->table);
		$is_insert = $this->save_is_insert($conds, $options);

		$sql_data = $this->save_pre_data($data, $is_insert, $options);

		$id = NULL;
		$is_assigned_id = false;

		$action = 'insert';

		if ($is_insert) {
			
			$info = $this->assign_insert_record_id($data, $sql_data);

			if(isset($info['is_assigned_id'])){
				$is_assigned_id = $info['is_assigned_id'];
			}

			if(isset($info['id'])){
				$id = $info['id'];
			}
			
			foreach($sql_data as $field => $val){
				$this->getDb()->set($field, $val, $val !== NULL);
			}

		} else {
			$action = 'update';

			if (is_string($conds)) {
				$id = $conds;
				$this->getDb()->where($this->pk_field, $id);
			} else {

				$this->selectingOptions($conds);

				if (isset($conds[$this->pk_field ])) {
					$id = $conds[$this->pk_field];
				} elseif (isset($data[ $this->pk_field ])) {
					$id = $data[$this->pk_field];
				} elseif (isset($sql_data[ $this->pk_field ])) {
					$id = $sql_data[$this->pk_field];
				}

			}

			if(isset($sql_data[ $this->create_date_field ])){
				unset($sql_data[$this->create_date_field]);
			}

			if(isset($sql_data[$this->create_by_field])){
				unset($sql_data[$this->create_by_field]);
			}

			if(isset($sql_data[$this->create_by_id_field])){
				unset($sql_data[$this->create_by_id_field]);
			}

		}

		//$this->getDb()->limit(1);
		$result = $this->getDb()->$action($table, $sql_data);

		// if auto_increment is on
		// getting the insert id from query
		if ($action == 'insert' && !$is_assigned_id && $this->auto_increment) {
			$data[$this->pk_field] = $id = $this->getDb()->insert_id();
		}

		$sql = $this->getDb()->last_query();

		$this->post_save_action($id);

		$output = compact('is_insert', 'result', 'sql', 'id');

		if ($this->_log_query) {
			log_message('error', __METHOD__ . '[' . $this->table . ']' .':'.__LINE__. ", \r\nDATA=" . print_r($sql_data, true) . "\r\nOUTPUT=" . print_r($output , true));
		}

		return $output;
	}

	protected function assign_insert_record_id( &$data, &$sql_data){
		$is_assigned_id = false;
		$id = null;
		if (!empty($data[$this->pk_field])) {
			$id                  = $data[$this->pk_field];
			$sql_data[$this->pk_field] = $id;
			$is_assigned_id      = true;
			// if auto_increment is off
			// generate id by getting the last value from current table
		} elseif ($this->use_guid) {
			if (!function_exists('guid')) {
				$this->load->helper('guid');
			}
			$id                  = guid();
			$is_assigned_id = TRUE;
			$sql_data[$this->pk_field] = $data[$this->pk_field] = $id;
		}elseif (!$this->auto_increment) {
			$is_assigned_id = TRUE;
			$id                  = $this->get_last() + 1;
			$sql_data[$this->pk_field] = $data[$this->pk_field] = $id;

			// if use_guid, <code>guid()</code> will be identified as the record's id
		}

		return compact('is_assigned_id', 'id');
	}

	protected function post_save_action($id) {
		// TODO: should be override
	}

	protected function save_is_insert($conds = false, $options = false) {
		if (!$conds || empty($conds)) {
			return TRUE;
		}

		return FALSE;
	}

	protected function save_pre_data($data, $is_insert = true, $options = false) {

		$sql_data = NULL;

		$def_vals = $this->new_default_values();

		// only listed variables are able to be executed by sql command
		if (!empty($this->fields)) {
			$sql_data = array();
			foreach ($this->fields as $idx => $field_name) {
				if (isset($data[$field_name])) {
					$sql_data[$field_name] = $data[$field_name];
				} elseif (isset($def_vals[$field_name]) && $is_insert) {
					$sql_data[$field_name] = $def_vals[$field_name];
				}

				// For date and datetime type, set to null if empty string or 0000 started value.
				if (isset($sql_data[$field_name])) {
					$field_info = $this->fields_details[ $field_name];

					$field_type = strtolower(data('type', $field_info));
					$field_is_nullable = strtolower(data('null', $field_info, false));

					$value_is_null = is_null($sql_data[$field_name]) || $sql_data[$field_name] === '';

					if($value_is_null){
						
						if($field_type == 'date' || $field_type == 'datetime'){
							if($field_is_nullable){
								$sql_data[$field_name] = null;
							}
						}
					// If the null value given, handle the null processing
						
						if($field_is_nullable){
							if($is_insert){
								unset($sql_data[$field_name]);
							}else{
								$sql_data[$field_name] = null;
							}
						}else{
							$sql_data[$field_name] = '';
						}
					}
				}
			}

		} else {
			$sql_data = $data;
		}

		$this->save_pre_data_attr($sql_data, $is_insert, $options);
		
		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', Before save:' . "\r\ninfo=" . print_r(compact('is_insert', 'data', 'sql_data', 'options'), true));
		}

		return $sql_data;
	}

	protected function handle_nonspace_value_to_sql(&$sql_data, $is_insert = true, $options = false){
		if(!is_array($this->no_space_fields))return;

		$this->load->helper('inflector');

		foreach($this->no_space_fields as $field_name){

			if (isset($sql_data[$field_name])) {
				$sql_data[$field_name] = underscore($sql_data[$field_name]);
			}
		}
	}

	protected function handle_parameter_value_to_sql(&$sql_data, $is_insert = true, $options = false){

		foreach($this->serializable_fields as $field_key => $field_info){
			$field = is_string($field_info) ? $field_info : $field_key;
			
			if(isset($sql_data[$field])){
				$sql_data[$field] = $this->serializeData($field, $sql_data[$field]);
			}
		}
	}

	protected function handle_owner_attribute_value_to_sql(&$sql_data, $is_insert = true, $options = false){

		$_user_type =  empty( $options['_user_type']) ? 'unknown':  $options['_user_type'];
		$_user_id =  empty( $options['_user_id']) ? '':  $options['_user_id'];


		$now = time_to_date();

		if ($is_insert) {
			if(empty($sql_data[ $this->create_date_field ])){
				if (in_array( $this->create_date_field , $this->fields)) {
					$sql_data[ $this->create_date_field ] = $now;
				}
			}
			if(empty($sql_data[$this->create_by_field ])){
				if (in_array($this->create_by_field, $this->fields)) {
					$sql_data[$this->create_by_field]    = $_user_type;
				}
			}
			if(empty($sql_data[$this->create_by_id_field])){
				if (in_array($this->create_by_id_field, $this->fields)) {
					$sql_data[$this->create_by_id_field] = $_user_id;
				}
			}
		}

		if(empty($sql_data[$this->modify_date_field])){
			if (in_array($this->modify_date_field, $this->fields)) {
				$sql_data[$this->modify_date_field] = $now;
			}
		}

		if(empty($sql_data[$this->modify_by_field])){
			if (in_array($this->modify_by_field, $this->fields)) {
				$sql_data[$this->modify_by_field] = $_user_type;
			}
		}

		if(empty($sql_data[$this->modify_by_id_field])){
			if (in_array($this->modify_by_id_field, $this->fields)) {
				$sql_data[$this->modify_by_id_field] = $_user_id;
			}
		}
	}

	protected function save_pre_data_attr(&$sql_data, $is_insert = true, $options = false) {

		$this->handle_nonspace_value_to_sql($sql_data, $is_insert, $options);

		$this->handle_parameter_value_to_sql($sql_data, $is_insert, $options);

		$this->handle_owner_attribute_value_to_sql($sql_data, $is_insert, $options);

	}

	/**
	 * Get a property from a table
	 *
	 * @param      boolean  $field    The field
	 * @param      boolean  $options  The options
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function get($field = false, $options = false) {
		$this->initDbConn();

		$row = $this->read($options);
		if (!$field) {return $row;
		}

		if (!isset($row[$field])) {return NULL;
		}

		return $row[$field];
	}

	/**
	 * Read a record from a table
	 *
	 * @param      mixed    $options  The options
	 * @param      integer  $offset   The offset
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function read($options = null, $offset = 0) {
		$this->initDbConn();

		if ($offset < 0) {
			$total_record = $this->get_total($options);
			$this->getDb()->limit(1, $total_record + $offset);
		} else {
			$this->getDb()->limit(1, $offset);
		}
		$this->selectingOptions($options);

		$query = $this->getDb()->get();
		if (!$query) {
			if ($this->_log_query) {
				log_message('error', __METHOD__ . '[' . $this->table . ']' . ', query error:' . $this->getDb()->last_query());
			}

			return null;
		}
		if ($query->num_rows() < 1) {
			if ($this->_log_query) {
				log_message('info', __METHOD__ . '[' . $this->table . ']' . ', empty result when getting row with sql:' . $this->getDb()->last_query());
			}

			return null;
		}

		$rows = $this->resultQuery($query, $options);

		if(!empty($rows))
			return isset($rows[0]) ? $rows[0] : $rows;
		return null;
	}

	/**
	 * Delete record(s)
	 *
	 * @param      boolean  $options  The options
	 * @param      integer  $limit    The limit
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function delete($options = false, $limit = -1) {
		$this->initDbConn();

		$this->selectingOptions($options);

		if ($limit > 0) {
			$this->getDb()->limit($limit);
		}

		$query = $this->getDb()->delete();

		$this->getDb()->flush_cache();
		
		return $this->getDb()->affected_rows();
	}

	/**
	 * Delete record(s). A port of delete method
	 *
	 * @param      boolean  $options  The options
	 * @param      <type>   $limit    The limit
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function remove($options = false, $limit = -1) {
		return $this->delete($options, $limit);
	}

	/**
	 * Searches for the first match.
	 *
	 * @param      boolean  $options  The options
	 *
	 * @return     <type>   ( description_of_the_return_value )
	 */
	public function find($options = false) {
		$this->initDbConn();

		$this->selectingOptions($options);

		$query = $this->getDb()->get();

		if (!$query) {
			ob_start();
			debug_print_backtrace();
			$data = ob_get_clean();
			
			log_message('error', __METHOD__ . '[' . $this->table . ']' . ', query error:' . $this->getDb()->last_query()."\r\nBacktrace:".$data);
			

			return NULL;
		}
		if ($query->num_rows() < 1) {
			if ($this->_log_query) {
				log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', empty when getting maximum value with sql:' . $this->getDb()->last_query());
			}

			return NULL;
		}
		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', query success:' . $this->getDb()->last_query());
		}

		$this->getDb()->flush_cache();
		
		return $this->resultQuery($query, $options);
	}

	public function find_paged($offset = -1, $per_page = 30, $options = false)
	{
		return $this->findPaged($offset, $per_page, $options);
	}

	/**
	 * Find a list of record by paging options
	 *
	 * @param      integer    $offset    The offset
	 * @param      integer    $per_page  The per page
	 * @param      boolean    $options   The options
	 *
	 * @throws     Exception  (description)
	 *
	 * @return     array      ( description_of_the_return_value )
	 */
	public function findPaged($offset = -1, $per_page = 30, $options = false) {
		$this->initDbConn();


		$offset = intval($offset);
		$per_page = intval($per_page);
		if(!is_int($offset)){
			ob_start();
			debug_print_backtrace();
			$data = ob_get_clean();
			log_message('error', __METHOD__ . '[' . $this->table . ']' . ', invalid offset'."\r\nBacktrace:".$data);
			throw new Exception('Unmatched data type of requesting paging data.', -1);
		}

		if(!is_int($per_page)){
			ob_start();
			debug_print_backtrace();
			$data = ob_get_clean();
			log_message('error', __METHOD__ . '[' . $this->table . ']' . ', invalid per_page.'."\r\nBacktrace:".$data);
			throw new Exception('Unmatched data type of requesting paging data.', -1);
		}

		$total_record = $this->getTotal($options);

		$per_page = max(1, $per_page);

		$total_page = ceil($total_record / $per_page);
		$page       = floor($offset / $per_page) + 1;

		$this->selectingOptions($options);

		if ($offset >= 0) {
			$this->getDb()->limit($per_page, $offset);
		} else {
			$this->getDb()->limit($per_page);
		}
		$query = $this->getDb()->get();

		if (!$query) {
			if ($this->_log_query) {
				log_message('error', __METHOD__ . '[' . $this->table . ']' . ', query error:' . $this->getDb()->last_query());
			}

			return NULL;
		}
		if ($query->num_rows() < 1) {
			if ($this->_log_query) {
				log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', empty when getting maximum value with sql:' . $this->getDb()->last_query());
			}

			return NULL;
		}
		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', query success:' . $this->getDb()->last_query());
		}

		$result = new \Dynamotor\Data\FetchedResult();

		if ($total_page < 0) {
			$total_page = ceil($total_record / $per_page);
		}

		if ($page < 0) {
			$page = floor($offset / $per_page) + 1;
		}

		$result['data']         = $this->result_query($query, $options);
		$result['total_record'] = intval($total_record);
		$result['total_page']   = intval($total_page);
		$result['page']         = intval($page);

		$result['index_from'] = $offset;
		$result['index_to']   = min($offset + $per_page, $total_record);
		$result['offset']     = $offset;
		$result['limit']      = $per_page;

		$this->getDb()->flush_cache();

		return $result;
	}

	public function get_mapping_fields() {
		return $this->getMappingFields();
	}

	public function getMappingFields() {
		$fields = array();
		foreach ($this->mapping_field as $idx => $field_name) {
			if (in_array($field_name, $this->fields)) {
				$fields[] = $field_name;
			}
		}

		return $fields;
	}

	public function get_total($options = false)
	{
		return $this->getTotal($options);
	}

	/**
	 * Gets the total.
	 *
	 * @param      boolean  $options  The options
	 *
	 * @return     <type>   The total.
	 */
	public function getTotal($options = false) {
		$this->initDbConn();

		$this->selectingOptions($options);

		return intval($this->getDb()->count_all_results());
	}

	/**
	 * Gets the sum.
	 *
	 * @param      boolean  $options  The options
	 * @param      string   $field    The field
	 * @param      boolean  $table    The table
	 *
	 * @return     integer  The sum.
	 */
	public function get_sum($options = null, $field = null, $table = null) {
		return $this->getSum($options, $field, $table);
	}
	
	public function getSum($options = null, $field = null, $table = null) {
		$this->initDbConn();

		$this->selectingOptions($options);
		$this->getDb()->select_sum($field, 'value');

		if(!empty($table))
			$query = $this->getDb()->get($table);
		else
			$query = $this->getDb()->get();

		if (!$query) {
			if ($this->_log_query) {
				log_message('error', __METHOD__ . '[' . $this->table . ']' . ', query error:' . $this->getDb()->last_query());
			}

			return 0;
		}
		if ($query->num_rows() < 1) {
			if ($this->_log_query) {
				log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', empty when getting maximum value with sql:' . $this->getDb()->last_query());
			}

			return 0;
		}
		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', query success:' . $this->getDb()->last_query());
		}

		$row = $query->row_array();

		if (isset($row["value"])) {
			return intval($row["value"]);
		}

		return 0;
	}

	/**
	 * Gets the maximum.
	 *
	 * @param      boolean  $options  The options
	 * @param      string   $field    The field
	 * @param      boolean  $table    The table
	 *
	 * @return     integer  The maximum.
	 */
	public function get_last($options = null, $field = null, $table = null) {
		return $this->getMax($options, $field, $table);
	}
	public function getLast($options = null, $field = null, $table = null) {
		return $this->getMax($options, $field, $table);
	}
	public function get_max($options = null, $field = null, $table = null) {
		return $this->getMax($options, $field, $table);
	}
	public function getMax($options = null, $field = null, $table = null) {
		$this->initDbConn();

		if(empty($field)){
			$field = $this->pk_field;
		}


		$this->selectingOptions($options);
		$this->getDb()->select_max($field, 'value');

		if(!empty($table))
			$query = $this->getDb()->get($table);
		else
			$query = $this->getDb()->get();

		if (!$query) {
			if ($this->_log_query) {
				log_message('error', __METHOD__ . '[' . $this->table . ']' . ', query error:' . $this->getDb()->last_query());
			}

			return 0;
		}
		if ($query->num_rows() < 1) {
			if ($this->_log_query) {
				log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', empty when getting maximum value with sql:' . $this->getDb()->last_query());
			}

			return 0;
		}
		if ($this->_log_query) {
			log_message('debug', __METHOD__ . '[' . $this->table . ']' . ', query success:' . $this->getDb()->last_query());
		}

		$row = $query->row_array();

		if (isset($row["value"])) {
			return intval($row["value"]);
		}

		return 0;
	}


	public function result($data, $options = false) {

		$need_custom_mapping = true;
		// the way of mapping the key for each row, passing with '_pk_based' for pre-configured primary key field, '_id_based' for 'id' field or '_field_based' for custom field
		// otherwise, use the offset of the result as the mapping key
		$_target_field = NULL;
		if (isset($options['_pk_based']) && in_array($this->pk_field, $this->fields)) {
			$_target_field = $this->pk_field;
		} elseif (isset($options['_id_based']) && in_array('id', $this->fields)) {
			$_target_field = 'id';
		} elseif (isset($options['_mapping_based'])) {
			$_target_field = '_mapping';
		} elseif (isset($options['_field_based'])) {
			if(is_string($options['_field_based'])){
				if(in_array($options['_field_based'], $this->fields)){
					$_target_field = $options['_field_based'];
				}
			}elseif(is_array($options['_field_based'])){
				$counter = 0;
				foreach($options['_field_based'] as $_field){
					if(in_array($options['_field_based'], $this->fields)){
						$counter++;
					}
				}
				if($counter>= count($options['_field_based']) && $counter > 0){
					$_target_field = $options['_field_based'];
				}
			}
		} else {
			$need_custom_mapping = false;
		}

		if (!$need_custom_mapping) {
			return $data;
		}

		$_output = array();

		if ($data && is_array($data) && count($data) > 0) {

			foreach ($data as $idx => $row) {

				if($row instanceof Entity){
					$row->setModel($this);
				}

				if (!empty($row) && is_array($row) && !isset($options['_no_result_row'])) {
					$row = $this->result_row($row, $options);
				}

				// mapping to row
				if (!empty($_target_field)) {

					// If the target field is an array, we assume that is mulit-level grouped.
					if(is_array($_target_field)){
						$last_pointer = $output;
						foreach($_target_field as $idx=> $_field){
							if(!isset($last_pointer[$_field])){
								$last_pointer[$_field] = [];
							}
							$last_pointer = &$last_pointer[$_field];
							if($idx>= count($_target_field)){
								$last_pointer[$_field] = $row;
							}
						}
						
					}else{
						$_output[$_row[$_target_field]] = $row;
					}
				} else {
					$_output[$idx] = $row;
				}
			}
		}

		return $_output;
	}

	/**
	 * { function_description }
	 *
	 * @param      <type>  $query    The query
	 * @param      <type>  $options  The options
	 *
	 * @return     array   ( description_of_the_return_value )
	 */
	public function result_query($query, $options = null) {
		return $this->resultQuery($query, $options);
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $query
	 * @param [type] $options
	 * @return void
	 */
	public function resultQuery($query, $options = null) {
		$need_custom_mapping = true;
		// the way of mapping the key for each row, passing with '_pk_based' for pre-configured primary key field, '_id_based' for 'id' field or '_field_based' for custom field
		// otherwise, use the offset of the result as the mapping key
		$_target_field = NULL;
		$is_group      = false;

		if (isset($options['_pk_based']) && in_array($this->pk_field, $this->fields)) {
			$_target_field = $this->pk_field;
		} elseif (isset($options['_id_based']) && in_array('id', $this->fields)) {
			$_target_field = 'id';
		} elseif (isset($options['_mapping_based'])) {
			$_target_field = '_mapping';
		} elseif (isset($options['_field_based'])) {
			$_target_field = $options['_field_based'];
		} elseif (isset($options['_field_based_array'])) {
			$_target_field = $options['_field_based_array'];
			$is_group      = true;
		} else {
			$need_custom_mapping = false;
		}

		$return_type = '\Dynamotor\Data\Entity';
		if(isset($options['_return_type'])){
			$return_type = $options['return_type'];
		}

		$_output = array();
		$_total  = $query->num_rows();

		if ($query && $_total > 0) {

			$row = $query->first_row($return_type);

			for ($idx = 0; $idx < $_total; $idx++) {

				$output_row = $row;
				if (!empty($row)  && !isset($options['_no_result_row']) && (is_array($row) || $row instanceof Entity)) {
					$output_row = $this->resultRow($row, $options);
				}
				// mapping to row
				if ($need_custom_mapping && !empty($row[$_target_field])) {
					$key = $row[$_target_field];
					if ($is_group) {
						$_output[$key][] = $output_row;
					} else {

						$_output[$key] = $output_row;
					}
				} else {
					$_output[$idx] = $output_row;
				}
				$row = $query->next_row($return_type);
			}
		}

		return $_output;
	}

	public function serializeData($key, $val)
	{
		return $this->serializeValue($val);
	}

	public function encode_parameters($val)
	{
		return $this->serializeValue($val);
	}
	
	/**
	 * Undocumented function
	 *
	 * @param [type] $val
	 * @return void
	 */
	public function serializeValue($val){
		return json_encode($val, true);
	}


	public function unserializeData($key, $val)
	{
		return $this->unserializeValue($val);
	}

	public function decode_parameters($val)
	{
		return $this->unserializeValue($val);
	}

	/*
	* Decode the extra parameters 
	* @param String
	* Formatted string, it will try to decode in JSON format. if fail, will try to use php.net/unserialize for supporting old format.
	*/
	public function unserializeValue($val){
		try{
			return json_decode($val, TRUE);
		}catch(Exception $exp){
			return unserialize($val);	
		}
		return NULL;
	}

	public function result_row($row, $options = null) {
		return $this->resultRow ($row, $options);
	}

	public function resultRow($row, $options = null) {
		if (is_array($row) || $row instanceof Entity ) {
			$row['_mapping'] = $this->get_row_mapping($row, $options);

			foreach($this->serializable_fields as $field_key => $field_info){
				$field = is_string($field_info) ? $field_info : $field_key;

				if(is_string($field_info)){
					if(isset($row[$field_info])){
						$row['_'.$field] = $row[$field];
						$row[$field] = $this->unserializeData($field, $row[$field]);
					}
				}
			}
		}
		return $row;
	}

	public function get_row_mapping($row, $options = null) {
		return $this->getRowMapping($row, $options);
	}
	public function getRowMapping($row, $options = null) {
		$fields = array_reverse($this->getMappingFields());

		foreach ($fields as $idx => $field_name) {
			if (isset($row[$field_name]) && !empty($row[$field_name])) {
				return $row[$field_name];
			}
		}
		if (isset($row[$this->pk_field])) {
			// default mapping key
			return $row[$this->pk_field];
		}
		return NULL;
	}

	protected function _query_fields($conds, $all_fields = false, $options=false, $def_table=false){
		if(is_string($conds)){
			return $conds;
		}

		$db = $this->getDb();

		$_stmts = [];


		foreach ($conds as $key => $val) {
			// skip if the first character is underscore
			if(substr($key,0,1) == '_') continue;

			$is_field_name_valid = true;
			
			$_stmt = null;

			$_field = $key;
			if(!preg_match('#^[a-zA-Z\_].*$#', $_field)){
				$is_field_name_valid = false;
			}

			if($is_field_name_valid){


				$_operator = '=';
				if (strpos(trim($key), ' ') > 0) {
					$pair = explode(' ', $key, 2);
					$_field = $pair[0];
					$_operator = strtoupper($pair[1]);
				}


				$field_info = $this->_get_field_info($_field,$def_table);
				$field = $field_info['prefix'].$field_info['field'];

				$val_is_array = is_array($val);
				$val_is_null = is_null($val);

				//print_r(compact('field','_operator'));

				$_stmt = null;

				if (!$all_fields || in_array($field_info['field'], $all_fields)){

					if($val_is_null && in_array($_operator, array('IS','IS NOT') )){
						$_stmt = $this->_where_match($field, null, $_operator);

					}elseif (in_array( $_operator , array('<', '<=', '=', '!', '!=', '>', '>=', 'LIKE')) && !$val_is_array) {
						if($_operator == '!') $_operator = '!=';
						$_stmt = $this->_where_match($field, $val, $_operator);
					}elseif ($_operator == 'IN' || $_operator == '=') {
						$_stmt =$this->_where_match($field, $val, '=');
					}elseif ($_operator == 'NOT IN' || $_operator == '!=') {
						$_stmt =$this->_where_match($field, $val, '!=');
					} else{
						$_stmt =$this->_where_match($field, $val, null, $options);
					}
				}

				// handle inner or case
			}elseif(!empty($val)){

				// if it's array form. 
				if(is_array($val)){
					$_or = array();
					foreach($val as $_field2 => $_val2){
						$is_field_name_valid2 = true;
						if(!preg_match('#^[a-zA-Z\_].*$#', $_field2)){
							$is_field_name_valid2 = false;
						}

						if($is_field_name_valid2){
							
							$_operator = '=';
							if (strpos($_field2, ' ') > 0) {
								$pair2 = explode(' ', $_field2, 2);
								$_field2 = $pair2[0];
								$_operator = strtoupper($pair2[1]);
							}

							$field_info2 = $this->_get_field_info($_field2,$def_table);
							$field2 = $field_info2['prefix'].$field_info2['field'];


							$_or[] = $field2.' '.$_operator.' ' .$this->getDb()->escape($_val2);
						}else{
							$_or[] = $_val2;
						}

					}
					$_stmt = $this->_or( $_or );


				// if it's string form.
				}elseif(is_string($val)){
					$_stmt = $val;
				}

			}
			if(!empty($_stmt)){
				$_stmts[] = $_stmt;
			}
		}
		if(!empty($_stmts))
			return implode(' AND ', $_stmts);
		return '';

	}

	/**
	 * For building querying
	 *
	 * @param mixed $options
	 * @return void
	 */
	protected function selectingOptions($options = null) {

		$table = $this->_table($options);

		if (isset($options['_table'])) {
			$table = $options['_table'];
		}

		$db = $this->getDb();
		$db->from($table);

		// Advanced selecting method
		$selected_fields = array();
		if (isset($options['_select'])  ) {
			$selected_fields = is_string($options['_select']) ?  explode(',', $options['_select']) : $options['_select'];
		}

		$all_fields = $this->fields;
		if (empty($all_fields)) {
			$all_fields = array_keys($this->default_values);
		}

		if (isset($options['_fields'])) {
			$all_fields = $options['_fields'];
		}

		// Handling for JOIN Case
		if (isset($options['_join']) && is_array($options['_join'])) {

			if(empty($selected_fields))
				$selected_fields[] = $table.'.*';

			$_joins = $options['_join'];
			if(isset($options['_join']['table'])){
				$_joins = array();
				$_joins[] = $options['_join']['table'];
			}

			foreach($_joins as $join_info){

				// handling select select case from join
				if(!empty($join_info['select'])){ 
					if(is_string($join_info['select'])){
						$selected_fields = array_merge($selected_fields , explode(',',$join_info['select']));
					}elseif(is_array($join_info['select'])){
						$selected_fields = array_merge($selected_fields , $join_info['select']);
					}
				}
				
				$stmt_str = $this->_query_fields($join_info['on']);
				if(empty($stmt_str)) {
					$stmt_str = null;
				}

				if(!empty($join_info['prefix'])){
					$db->join($join_info['table'], $stmt_str, $join_info['prefix']);
				}else{
					$db->join($join_info['table'], $stmt_str);
				}
			}
		
		}

		if(isset($options['_group_by'])){
			$db->group_by($options['_group_by']);
		}

		if(!empty($selected_fields)){
			$_selected_fields = array();
			foreach($selected_fields as $field_key => $field_label){
				if(is_string($field_key) && !empty($field_key)){
					$_selected_fields[] = $field_key .' AS ' . $field_label;
				}else{
					$_selected_fields[] = $field_label;
				}
			}

			if(!empty($_selected_fields)){
				$db->select(implode(', ', $_selected_fields));
			}
		}

		if (is_array($options)) {
			$stmt = $this->_query_fields($options, $all_fields, $options);
			if($stmt !== null && !empty($stmt)){
				$db->where($stmt, null, null);
			}
		}


		if (isset($options['_keyword']) && isset($options['_keyword_fields'])) {
			$fields = $options['_keyword_fields'];
			$stmt = $this->_like_fields($fields, $options['_keyword']);
			if($stmt !== null && !empty($stmt)){
				$db->where($stmt, null, null);
			}
		}

		if (isset($options['_mapping'])) {

			$_fields = array($this->pk_field);
			$mapping_fields = $this->get_mapping_fields();
			if(!empty($mapping_fields)){
				foreach ($mapping_fields as $idx => $_mapping_field) {
					if(!in_array($_mapping_field, $_fields))
						$_fields[] = $_mapping_field;
				}
			}

			$stmt = array();
			foreach ($_fields as $idx => $key) {
				$_stmt = $this->_array_to_in_case($this->_field($key), $options['_mapping']);;
				if(!empty($_stmt)){
					$stmt[] = $_stmt;
				}
			}

			$stmt_str = $this->_or($stmt);
			if(!empty($stmt_str)){
				$db->where($stmt_str, null, null);
			}
		}

		if (isset($options['_order_by'])) {
			// To handle multiple tables query and prevent same field name when selecting
			// we have to knowing each setting and add table name in front of field name.
			$_order_by = $options['_order_by'];
			if (!is_array($options['_order_by'])) {
				$_order_by = array($options['_order_by']);
			}
			foreach ($_order_by as $key => $val) {

				if (!is_string($key)) {
					$_ary = explode(' ',$val);
					$key = $_ary[0];
					$val = '';
					if(!empty($_ary[1]))
						$val = $_ary[1];
				}
				$_field = $this->_field($key);
				if(!empty($_field)){
					$db->order_by($_field . ' ' . strtoupper($val) );
				}
			}
		}

		$this->selecting_options($options);
	}

	/**
	 * @deprecated 3.1.4
	 *
	 * @param boolean $options
	 * @return void
	 */
	protected function selecting_options($options = false) {
	}

/***************************************************************************************************/
// Protected functions

	protected function _or($conds = []) {
		return "(" . implode(" OR ", $conds) . ")";
	}

	protected function _like_fields($fields, $val, $is_not = false) {

		$filter_val = $this->getDb()->escape_like_str($val);
		$filter_val = preg_replace('#[\s\t]+#','%',$filter_val);
		$ary = array();
		if (isset($fields) && is_array($fields) && count($fields)>0) {
			foreach ($fields as $idx => $key) {
				$ary[] =  $this->_field($key,false,false,true). ($is_not ? ' NOT':'') ." LIKE '%" . $filter_val . "%'";
			}
			return $this->_or($ary);
		}
		return null;
	}

	protected function _table($options = true, $table_name = false) {
		if (!$table_name) {
			$table_name = $this->table;
		}

		return ($table_name);
	}

	protected function _field($field, $use_quote = false,$def_table=false,$use_prefix = false) {

		if(strpos($field,')') !==FALSE || strpos($field,'(') !==FALSE){
			return $field;
		}

		$_info = $this->_get_field_info($field,$def_table,$use_prefix);

		if(empty($_info)){
			return $field;
		}

		$field = $_info['field'];

		if (isset($this->fields_alias[$_info['field']])) {
			$field = $this->fields_alias[ $_info['field']];
		}
		return $this->_fill_table($_info['prefix'] .$field,$use_quote);
	}

	protected function _get_field_info($field,$def_table=false,$use_prefix=false){

		$prefix = '';
		$table = '';
		$suffix = '';

		if(strpos($field,' ')>0){
			$pairs = explode(' ',$field,2);
			$field = $pairs[0];
			$suffix = ' '.$pairs[1];
		}

		if(strpos($field,'.')>0){
			$pairs = explode('.',$field,2);
			$table = $pairs[0];
			$field = $pairs[1];
		}elseif(!empty($def_table)){
			$table = $def_table;
		}else{
			if(in_array($field, $this->fields)){
				$table = $this->table;
			}

		}
		if(!empty($table)){
			$prefix =  $use_prefix ? $this->getDb()->dbprefix($table) : $table;
			$prefix.='.';
		}
		return compact('prefix','table','field','suffix');
	}

	protected function _fill_table($field, $use_quote = true){

		$info =$this->_get_field_info($field);

		if(!in_array($field,$this->fields)){
			return $info['prefix'].($use_quote ? $this->_element_quoted($info['field']) : $info['field']);
		}
		
		return $info['prefix'].($use_quote ? $this->_element_quoted($info['field']) : $info['field']);
	}

	// for mysql, use ` for query as table/field name
	protected function _element_quoted($field) {
		return '`' . $field . '`';
	}
	
	protected function _where_match($field, $vals = '', $operator = '=', $options = null) {

		if (is_array($vals)) {
			return '(' . $this->_array_to_in_case($field, $vals, $operator, $options) . ')';

		} else {
			$field_name   =$this->_field($field);
			return $field_name ." ".$operator." ".$this->get_db()->escape($vals)."";
			// $this->get_db()->where($field_name, $vals);
		}
	}

	protected function _array_to_in_case($field, $vals = [], $operator = 'IN', $options = null) {
		if (!is_array($vals)) {
			$vals = array($vals);
		}
		if( count($vals) < 1){
			log_message('error', __METHOD__.'['.$this->table.'] '.$field.' does not include any values for in the array.'."\r\nBacktrace:\r\n".print_r(debug_backtrace(),true));
			
			// TODO: Undefined exception code
			throw new \Exception($field.' does not include any values in the array.');
		}

		// swap operator
		if($operator == '='){
			$operator = 'IN';
		}

		if($operator == '!='){
			$operator = 'NOT IN';
		}

		$vals_ary = array();
		foreach ($vals as $idx => $val) {
			$vals_ary[] = $this->get_db()->escape($val);
		}

		$field_name   =$this->_field($field);
		return $field_name.' '.$operator.' (' . implode(", ", $vals_ary) . ')';
	}
}
