<?php 
namespace Dynamotor\Core;
use \CI_Config;

class HC_Config extends CI_Config {


	public function __construct(){
		parent::__construct();

		// reset base url
		$base_url = $this->base_url();
		$this->set_item('base_url', $base_url);
	}

	public function get_script_path(){
		return substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
	}

	public function base_url($uri = '', $protocol= NULL)
	{
		// if output does not a http-protocol format url, change it
		if(!preg_match('#^[a-z0-9]{1,}\:\/\/.+#',$uri)){

			$path = $uri;

			$prefix = '';

			$protocol = 'http';

			if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') 
				$protocol = 'https';

			if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']))
				$protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];

			$host = 'localhost';
			if(isset($_SERVER['HTTP_HOST']))
				$host= $_SERVER['HTTP_HOST'] ;
			$port = '';



			//if(substr($uri,0,1) != '/'){
				$prefix =$this->get_script_path();
			//}

			$uri = $protocol.'://'.$host.$port.$prefix. $path;
		}
		return $uri;
	}

	public function is_require_https()
	{
		return $this->item('require_https') == true;
	}

	public function site_url($uri = '', $protocol = NULL, $options = NULL)
	{

		// if output does not a http-protocol format url, change it
		if(has_scheme($uri))
			return $uri;

		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}
		
		$prefix = $this->item('index_page');
		$base_url = $this->base_url($prefix, $protocol);

		if(empty($options) || (isset($options['localize']) && $options['localize'])){
			$LANG = $GLOBALS['LANG'];

			$old_uri = $uri;
			$uri = $LANG->localize_url($uri);
			
			$tmp_locale_info = $LANG->parse_url($uri);
			//log_message('debug',get_class($this).'/site_url, '.$old_uri.' updated to '.$uri);
			
			
			if(isset($tmp_locale_info['locale'])){
				$locale_info = $LANG->get_locale($tmp_locale_info['locale']);
				
				if(isset($locale_info['host']) && !empty($host) && $locale_info['host'] != $host){
					$base_url = $locale_info['host'];
					$base_url = $protocol. '://'.preg_replace('/\/+/', '/', $base_url);
					
				}
			}
		}
		if(substr($uri,0,1) == '/' &&substr($base_url,-1,1) == '/' )
			$uri = substr($uri,1, strlen($uri)-1);

		$final_url = $base_url.$uri;
		// log_message('error',__METHOD__.'@'.__LINE__.', data='.print_r(compact('base_url','uri','protocol','tmp_locale_info','locale_info','final_url'), true));
		return $final_url;
		// return parent::site_url($uri, $protocol);
	}

	public function is_https()
	{
		$protocol = 'http';

		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') 
			$protocol = 'https';

		if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']))
			$protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];

		return $protocol == 'https';
	}

	public function fix_http_url($path)
	{
		if(has_scheme($path)) return $path;

		if( substr($path,0,2) == '//'){
			if( $this->is_https() ){
				$path = 'https:'.$path;
			}else{
				$path = 'http:'.$path;
			}
		}
		return $path;
	}

	public function url($path='', $type = 'base_url', $options = null){
		//return $path;
		$path = stext($path);
		$path = $this->fix_http_url($path);
		if( has_scheme($path) ) return $path;

		
		$CI = &get_instance();
		
		$_url = $CI->config->item($type);
		if(empty($_url)){
			$_url = base_url();
		}

		if(substr($_url,0,2) == '//'){
		}elseif(! has_scheme($_url) ){
			if(substr($_url,0,1) == '/'){
				$_url = ('//'.$_SERVER['HTTP_HOST'].$_url);
			}
		}
		$_url = $this->fix_http_url($_url);


		if(substr($_url,-1,1) !='/' && strlen($path)> 0 && substr($path,0,1)!='/'){
			$_url.='/';
		}

		return ($_url.$path);
	}

	public function web_url($uri = '', $options = NULL)
	{	
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}
		
			if(empty($options) || (isset($options['localize']) && $options['localize'])){
				$LANG = $GLOBALS['LANG'];
				$old_uri = $uri;
				$uri = $LANG->localize_url($uri);
				
				$tmp_locale_info = $LANG->parse_url($uri);
				
				//log_message('debug',get_class($this).'/web_url, '.$old_uri.' updated to '.$uri);
				if(isset($tmp_locale_info['locale'])){
					$locale_info = $LANG->get_locale($tmp_locale_info['locale']);
					
					
					if(isset($locale_info['host']) && !empty($host) && $locale_info['host'] != $host){
						$new_uri = $locale_info['host'].'/'.$uri;
						$new_uri = '://'.preg_replace('/\/+/', '/', $new_uri);
						if(substr($new_uri,-1,1) == '/')
							$new_uri = substr($new_uri,0, strlen($new_uri)-1);
						
						return $new_uri;
					}
				}
			}

		return $this->url($uri, 'web_url');
	}

	public function theme_url($path=''){

		$theme = $this->item('theme');

		$url_prefix = $this->item('theme_url');
		if(empty($url_prefix)){
			$url_prefix = $this->item('base_url');
			if(!has_tail_slash($url_prefix))
				$url_prefix .='/';
			$url_prefix .= 'assets/themes/';
		}
		if(!has_tail_slash($url_prefix))
			$url_prefix .='/';
		if(!empty($theme))
			$url_prefix .= $this->item('theme').'/';
		
		return $this->url($url_prefix.$path,'base_url');
	}

}

// END MY_Config Class

/* End of file MY_Config.php */
/* Location: ./system/application/libraries/MY_Config.php */
