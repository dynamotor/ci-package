<?php 

namespace Dynamotor\Extended\Modules;

use \Dynamotor\Core\HC_Module;

class RouteModule extends HC_Module
{
	public $routes = [];

	public function __construct($config=  NULL){

		parent::__construct();

		$this->initialize($config);

		log_message('debug',get_class($this).' initialized.');
	}

	public function initialize($config = null){

		if(isset($config['routes'])){
			$this->routes = $config['routes'];
		}
	}

	public function add($pattern , $callable , $matching = null, $request_method = null){

		$this->routes[] = compact('pattern', 'callable', 'matching', 'request_method');

		// print_r($this->routes[ $pattern ]);

		// if($this->is_debug()) log_message('debug',get_class($this).'/add, '.print_r(compact('pattern','callable','matching','request_method'), true));
		return $this;
	}

	public function remove($pattern){
		unset($this->routes[$pattern ]);

		return $this;
	}

	public function run($uri_string = ''){
		return $this->do_route($uri_string, $this->routes);
	}

	public function reset(){
		$this->routes = array();
		return $this->routes;
	}

	var $routed_instance;

	public function do_route($uri_string, $routes, $route_prefix = ''){

		if( !is_array($routes)) return FALSE;
		
		$input_request_method = strtoupper($this->input->request_method());

		if($this->is_debug())
			log_message('debug',get_class($this).'/do_route, started for parsing at "'.$uri_string.'".');

		$cache_key = 'route_mod/app_'.basename(APPPATH).'/hashed/'.$input_request_method.'/'.md5($uri_string);
		$cached_route = cache_get($cache_key);

		if(!empty($cached_route) && isset($cached_route['index'])){
			$index = $cached_route['index'];
			$pattern = $cached_route['pattern'];
			$route_matches = $cached_route['route_matches'];
			$route_setting = $routes[$index];

			if(isset($routes[$index])){
				return $this->perform_route($index, $uri_string, $pattern, $route_matches, $route_setting);
			}
		}

		foreach($routes as $index => $route_setting){
			$route_pattern = $route_setting ['pattern'];
			if(!empty($route_pattern) && substr($route_pattern,-1,1) !='/') $route_pattern = $route_pattern.'\/';
			$pattern = $route_prefix.$route_pattern;

			if(substr($pattern,-1,1) == '/')
				$pattern.='?';
			$pattern = '#^'.$pattern.'$#';


			$_request_method = isset($route_setting['request_method']) ? strtoupper($route_setting['request_method']) : NULL;

			if(!empty($_request_method) &&  $input_request_method != $_request_method){
				log_message('debug',get_class($this).'/do_route, skipped pattern '.$pattern.' for unmatched http_method: '.$_request_method);
				continue;
			}

			// find it by regexp
			$matched = preg_match($pattern, $uri_string, $route_matches );
			$is_matched = $matched > 0;
			$is_success = false;


			if($this->is_debug())
				log_message('debug',get_class($this).'/do_route, result at index '.$index.', pattern: '.$pattern.': '.print_r(compact('matched','is_success'), true));

			if($is_matched ){

				$result = $this->perform_route($index, $uri_string, $pattern, $route_matches, $route_setting);


				if($result){
					cache_set($cache_key, compact('index', 'uri_string','pattern','route_matches'));
					return true;
				}
			}

		}

		return false;
	}

	/**
	 * Determine if the route config matched
	 *
	 * @param int $index
	 * @param string $uri_string
	 * @param string $pattern
	 * @param mixed $route_matches
	 * @param mixed $route_setting
	 * @return boolean
	 */
	protected function perform_route($index, $uri_string, $pattern, $route_matches, $route_setting ){
		
		$_callable = &$route_setting['callable'];
		$_matching = $route_setting['matching'];
		$_request_method = isset($route_setting['request_method']) ? strtoupper($route_setting['request_method']) : NULL;


		$_method = null;
		$_arguments = $_matching;



		$route_replace_callback = function( $arg_matches) use($pattern, $route_matches, $route_setting){
			// print '$pattern='.print_r($pattern,true)."\r\n\r\n";
			// print '$route_setting='.print_r($route_setting,true)."\r\n\r\n";
			// print '$route_matches='.print_r($route_matches,true)."\r\n\r\n";
			// print '$arg_matches='.print_r($arg_matches,true)."\r\n\r\n";
			if(isset($route_matches[ $arg_matches[1] ])){
				return $route_matches[ $arg_matches[1] ];
			}
		};

		// $_method = preg_replace_callback('#\\$([0-9]+)#',$route_replace_callback , $_method);

		if(is_string($_callable) && class_exists($_callable)){

			if(!isset($this->routed_instance[$_callable ])){
				$this->routed_instance[$_callable ] = new $_callable();
			}

			$_callable = $this->routed_instance[ $_callable ];
			
			$_method = array_shift($_matching);
			$_arguments = $_matching;
			$_callable = array($_callable , $_method);

		}elseif( !is_callable($_callable)){
			$_method = array_shift($_matching);
			$_arguments = $_matching;
			$_callable = array($_callable , $_method);
		}

		// if this class has this method, do this
		if( is_callable( $_callable) ){
			$_args = array();

			$_output = NULL;

			// prepare argument list
			if(is_array($_arguments)){
				foreach($_arguments as $index => $argument_info){
					if(is_string($argument_info)){

						$_output = preg_replace_callback('#\\$([0-9]+)#',$route_replace_callback , $argument_info);
					}elseif(is_array($argument_info)){

						$_output = array();
						foreach($argument_info as $arg_key => $arg_val){
							$_output[ $arg_key] = preg_replace_callback('#\\$([0-9]+)#', $route_replace_callback, $arg_val);
						}
					}
					$_args[] = $_output;
				}
			}

			$is_success = true;

			$result  = call_user_func_array($_callable, $_args );

			return $result !== false;
		
		}else{
			log_message('error',get_class($this).'/do_route, matched route is not callable');
			return false;
		}
	}

}