<?php 

namespace Dynamotor\Extended\Controllers;

use \Dynamotor\Core\HC_Module;

/**
 * This class is an adapter to work with HC_Controller
 */
class ModuleController extends HC_Module
{
	public $routes = [];
	public $route_prefix = '';


	protected function _get_default_vals($action='index', $vals= array())
	{
		//$vals ['is_debug'] = $this->_is_debug();
		return $vals;
	}


	protected function perform_custom_route()
	{

		// get the current uri_string
		$uri_string = urldecode(uri_string());
		$uri_string = preg_replace('#\.'.$this->uri->extension().'$#','', $uri_string);

		$directory = $this->route_prefix;
		if(substr($directory,-1,1) != '/') $directory.='/';
		$prefix_pattern = !empty($directory) ? str_replace("/", "\\/", $directory): "";
		

		// stop here if route successfully
		if($this->do_route($uri_string, $this->routes, $prefix_pattern ))
			return true; 

		//exit;
		return false;
	}

	protected function do_route($uri_string, $routes = NULL, $route_prefix = NULL)
	{

		if( !is_array($routes)) return FALSE;

		foreach($routes as $route_pattern => $route_setting){
			if(!empty($route_pattern) && substr($route_pattern,-1,1) !='/') $route_pattern = $route_pattern.'\/';
			$pattern = $route_prefix.$route_pattern;

			if(substr($pattern,-1,1) == '/')
				$pattern.='?';
			$pattern = '#^'.$pattern.'$#';

			// find it by regexp
			$matched = preg_match($pattern, $uri_string, $route_matches );
			$is_matched = $matched > 0;
			$is_success = false;
			if($is_matched ){
				$_method = array_shift($route_setting);



				$route_replace_callback = function( $arg_matches) use($route_matches){
					//print '$arg_matches='.print_r($route_matches[ $arg_matches[1] ],true)."\r\n\r\n";
					return $route_matches[ $arg_matches[1] ];
				};

				$_method = preg_replace_callback('#\\$([0-9]+)#',$route_replace_callback , $_method);

				// if this class has this method, do this
				if(method_exists($this, $_method)){
					$_args = array();

					$arguments = $route_setting;
					$_output = NULL;

					// prepare argument list
					if(is_array($arguments)){
						foreach($arguments as $index => $argument_info){
							if(is_string($argument_info)){

								$_output = preg_replace_callback('#\\$([0-9]+)#',$route_replace_callback , $argument_info);
							}elseif(is_array($argument_info)){

								$_output = array();
								foreach($argument_info as $arg_key => $arg_val){
									$_output[ $arg_key] = preg_replace_callback('#\\$([0-9]+)#', $route_replace_callback, $arg_val);
								}
							}
							$_args[] = $_output;
						}
					}

					$is_success = true;

					if($this->input->get('debug')!='test-route'){
						call_user_func_array(array($this, $_method), $_args );

						return true;
					}
				}
			}
			if($this->input->get('debug')=='test-route'){
				$LANG = $this->lang;
				var_dump(compact('pattern','is_matched','is_success','route_pattern','route_matches','route_setting', '_method','_args', '_output'));
			}
		}


		if($this->input->get('debug')=='test-route'){
			return true;
		}

		return false;
	}
	
	public function _remap(){
		if($this->perform_custom_route()){
			return;
		}

		return $this->_show_404('subroute_not_matched');
	}

	// return FALSE which is Allowed.
	public function _restrict($scope = NULL,$redirect=true){
		return get_instance()->_restrict($scope, $redirect);
	}

	protected function singleton($classpath, $parameters = null, $is_shared = true, $alias = null){
		return $this->load->singleton($classpath, $parameters, $is_shared, $alias);
	}
	
	public function _permission_denied($scope=NULL){
		return $this->error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG, 401, compact('scope'));
	}
	
	public function _is_debug(){
		return get_instance()->_is_debug();
	}

	public function _is_ext($group='html'){
		return $this->request->is_support_format($group);
	}

	public function _is_extension($group='html'){
		return $this->request->is_support_format($group);
	}

	public function _system_error($code, $message = 'Unknown system error.', $status=500, $data = NULL){
		return get_instance()->_system_error($code, $message, $status, $data);
	}
	
	public function _api($vals, $default_format = 'json') {
		return get_instance()->_api($vals, $default_format);
	}
	
	public function _error($code, $message = '', $status = 500, $data=NULL) {
		return get_instance()->_error($code, $message, $status, $data);
	}

	public function _show_404($message = 'unknown') {
		return get_instance()->_show_404($message);
	}

	public function _render($view, $vals = false, $layout = false, $theme = false) {
		return get_instance()->_render($view, $vals, $layout, $theme);
	}

}