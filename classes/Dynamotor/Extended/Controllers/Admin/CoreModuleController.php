<?php
namespace Dynamotor\Extended\Controllers\Admin;

use Dynamotor\Extended\Controllers\ModuleController;

class CoreModuleController extends ModuleController
{

	var $user_type = 'admin';

	protected function _get_editor_info(){
		if(isset($this->adminService->auth))
			return array('_user_type'=>'admin', '_user_id'=>$this->adminService->auth->get_id());
		return array('_user_type'=>'unknown','_user_id'=>NULL);
	}
	
	public function _render($view, $vals=false, $layout=false, $theme =false){


		if ($this->input->get('dialog') == 'yes' || $this->input->get('dialog') == 'true' || $this->input->get('dialog') == '1') {
			$vals['is_dialog'] = TRUE;
			$this->asset->add_data('body_css_class', 'dialog');
			if (!$layout) {
				$layout = 'dialog';
			}
		}

		return parent::_render($view, $vals, $layout, $theme);
	}

}
