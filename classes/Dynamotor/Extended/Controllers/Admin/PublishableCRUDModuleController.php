<?php 

namespace Dynamotor\Extended\Controllers\Admin;

use \Dynamotor\Extended\Models\PublishableModel;
use \Dynamotor\Core\HC_Exception;

use \Dynamotor\Extended\Helpers\UniqueIDHelper;
use \Dynamotor\Extended\Models\RelationshipModel;

/**
 * A cloned version of PublishedCRUDController
 */
class PublishableCRUDModuleController extends CRUDModuleController
{
	protected $model_class_path = '';
	protected $_published_model;

	protected function _prepare_model(){

		$this->_target_model = null;
		$this->_published_model = null;
		if(class_exists($this->model_class_path)){
			$this->_target_model = new $this->model_class_path(array('mode'=>PublishableModel::MODE_STAGING ));
			$this->_published_model = new $this->model_class_path(array('mode'=>PublishableModel::MODE_PUBLISHED));
		}
		// if($this->localized){
		// 	$this->load->model('text_locale_model');
		// }
	}


	public function status_enable($record){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_PROPERTY_CHANGE'), FALSE)){
			return FALSE;
		}
		
		$query_opts = $this->_select_options('status', array($this->_target_model->pk_field=>$record[$this->_target_model->pk_field]));
		
		$this->_target_model->save(array('status'=>'1'), $query_opts);

		return TRUE;
	}

	public function status_disable($record){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_PROPERTY_CHANGE'), FALSE)){
			return FALSE;
		}
		
		$query_opts = $this->_select_options('status', array($this->_target_model->pk_field=>$record[$this->_target_model->pk_field]));
		
		$this->_target_model->save(array('status'=>'0'), $query_opts);

		return TRUE;
	}


	protected function _select_options($action = 'default', $options=false){
		$options = parent::_select_options($action, $options);

		if($action != 'delete' && $action != 'status'){
			if($this->localized && empty($options['locale'])){
				$options['locale'] = $this->lang->locale();
			}
		}

		return $options;
	}


	protected function _search_options($options=false){
		$options = parent::_search_options($options);


		if($this->localized && empty($options['locale'])){
			$options['locale'] = $this->lang->locale();
		}

		return $options;
	}
	
	public function save($id=false){
		
		if( $this->_restrict($this->_get_permission_scopes('RECORD_SAVE'))){
			return FALSE;
		}

		$ref_table = isset($this->_target_model->base_table) ? $this->_target_model->base_table : $this->_target_model->table;

		$editor_info =$this->_get_editor_info();
		
		$vals = $this->_get_default_vals('save');
		$success = true;
		
		if(!$id) $id = $this->input->get_post('id');

		$action = !$id ? 'add' : 'edit';
 

		if($action == 'add' && !$this->add_enabled){
			return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
		}

		if($action == 'edit' && !$this->edit_enabled){
			return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
		}

		if($action == 'add'){
			if(isset($this->_target_model->use_global_id) && $this->_target_model->use_global_id){
				$id = UniqueIDHelper::shared()->get_id( $ref_table );

				log_message('debug', __METHOD__.'@'.__LINE__.', new id created: '.$id);
			}
		}elseif($action == 'edit'){

			$record = $old_record = NULL;
			if(!empty($id)){

				$query_opts = $this->_select_options('editor',array( 
					$this->_target_model->pk_field => $id, 
				));
				$old_record = $record = $this->_target_model->read($query_opts) ;
				if(!isset($record[$this->_target_model->pk_field]) || $record[$this->_target_model->pk_field] != $id){
					return $this->_show_404('record_id_not_matched');
				}
			}
		}

		try{
			if($this->localized){

				$locale_keys = $this->lang->get_available_locale_keys();

				foreach($locale_keys as $locale_key){
					$saved_data[ $locale_key] = $this->_save_row($action, $id, $locale_key);
				}

				// Necessary for localied table.
				$this->_after_save($action, $id, null, null, null, $vals);

			}else{
				$saved_data = $this->_save_row($action, $id, NULL);
			}
		}catch(HC_Exception $exp){
			return $this->_error($exp->code, $exp->getMessage(), 200, $exp->data);	
		}
		
		$vals['id'] = $id;
		$vals['method'] = $action;
		$vals['data'] = array();

		if($this->localized){

			$vals['loc'] = $saved_data;
			if(isset($save_data[ $this->lang->locale () ])){
				$vals['data'] = $save_data[ $this->lang->locale () ];
			}
		}else{
			$vals['data'] = $saved_data;
		}


		$query_opts = $this->_select_options('editor',array( 
			$this->_target_model->pk_field => $id, 
		));
		$record = $this->_target_model->read($query_opts) ;

		foreach($this->editable_fields as $idx => $field_name)
			$vals['data'] [ $field_name ] = data($field_name, $record);

		if(isset($this->_target_model) )
			$vals['queries'] = $this->_target_model->get_db()->queries;

		if($this->uri->is_extension('')){
			redirect($this->view_prefix.$this->view_scope.'/'.$this->view_type.'/'.$id);
			return;
		}
		if($this->_is_ext('data')){
			return $this->_api($vals);
		}
		return $this->_show_404();
	}

	protected function _save_row($action, $id, $locale = NULL){

		$editor_info = $this->_get_editor_info();

		$query_opts = array( $this->_target_model->pk_field =>$id);

		$query_opts = $this->_select_options('save', $query_opts);

		// override locale request
		if(!empty($locale))
			$query_opts['locale'] = $locale;

		log_message('debug',  __METHOD__.'@'.__LINE__.', data='.print_r(compact('query_opts','action','id','locale'), true));

		$data = array();
		$loc_data = array();

		$old_record = $record = $this->_target_model->read($query_opts) ;

		$last_query = $this->_target_model->get_db()->last_query();
		log_message('debug',  __METHOD__.'@'.__LINE__.', data2:'.print_r(compact('old_record', 'id', 'last_query'), true));

		$success = $this->_before_save($action, $record, $data, $loc_data, $locale);
		
		if( ! $success ){
			log_message('error', __METHOD__.'@'.__LINE__.': cannot save');
			throw new HC_Exception(-1, 'save_error', compact('id','locale','data') );
		}



		$validate = array();
		if(!$this->_validate_save($query_opts, $old_record, $data, $validate)) {
			throw new HC_Exception(-1, 'validation_error', compact('validate','id','locale','data') );
		}

		if(empty($record[ $this->_target_model->pk_field ]) || $action == 'add'){
			if($this->_target_model->has_field('locale'))
				$data['locale'] = $locale;
			$data[ $this->_target_model->pk_field ] = $id;
			
			$result = $this->_target_model->save($data,NULL, $editor_info);

			$query_opts = $this->_select_options('save', array($this->_target_model->pk_field=>$result['id']));
			$id = $result['id'];
		}else{
			unset($data[$this->_target_model->publish_field ]);
			unset($data[$this->_target_model->pk_field]);
			unset($data[$this->_target_model->create_date_field]);
			unset($data[$this->_target_model->create_by_field]);
			unset($data[$this->_target_model->create_by_id_field]);
			$result = $this->_target_model->save($data, $query_opts, $editor_info);
		}

		$record = $this->_target_model->read($query_opts);

		$this->_clear_cache($record);

		if($locale != null){
			$this->_after_save_localized($action, $id, $old_record, $data, $loc_data, $vals, $locale);
		}else{
			$this->_after_save($action, $id, $old_record, $data, $loc_data, $vals);
		}
		
		return $data;
	}

	protected function _before_save($action, $record, &$data=false, &$loc_data=false , $locale = NULL ){

		$defvals = $this->_target_model->new_default_values();
		$loc_data = $this->input->post('loc');

		foreach($defvals as $field=>$val){

			// Ignore fields if the default values does not allowed for save action
			if(!empty($this->editable_fields)){
				if(!in_array($field, $this->editable_fields))
					continue;
			}
			$post_value = $this->input->post($field);

			if($this->localized){

				if(isset($this->_target_model->fields_details[ $field ]['localized']) && $this->_target_model->fields_details[ $field ]['localized']){
					// prepare data for localized content
					if(isset($loc_data[$locale][ $field ]))
						$post_value = $loc_data[$locale][$field];
				}
			}

			if(!isset($data[$field]))
				$data[$field] = $val;
			if(isset($record[$field])) 
				$data[$field] = $record[$field];
			if($post_value !== NULL && $post_value !== FALSE) 
				$data[$field] = $post_value ;

			// handle duplicated value for unique_value
			if(isset($this->_target_model->fields_details[$field]['unique_value']) ){

				switch($this->_target_model->fields_details[$field]['unique_value']){
					case 'fill_date':

						// only handle when value is not null
						if(!empty($data[$field])){
							$found_query = array(
								$field => $data[$field],
							);
							if($action == 'edit'){

								if(!empty($record[$this->_target_model->pk_field])){
									$found_query[$this->_target_model->pk_field.' !='] = $record[$this->_target_model->pk_field];
								}
							}

							if($this->localized){
								$found_query['locale'] = $locale;
							}

							log_message('debug', __METHOD__.'@'.__LINE__.': checking with unique column '. $field.' at model '.get_class($this->_target_model));
							$num_found = $this->_target_model->get_total($found_query);
							if($num_found > 0){
								$val = $data[$field];

								if(preg_match('#.+-[0-9]{14}$#', $val)){
									$val = substr($val,0, strlen($val) - 15);
								}
								if(strlen($val) < 1)
									$val = $this->_target_model->table;

								$data[$field] = $val. '-'.time_to_date('Ymdhis');
							}
						}
						break;
				}
			}
		}

		return TRUE;
	}

	protected function _before_save_localized($action, $record, &$data=false, &$loc_data=false  , $locale = NULL)
	{

	}

	protected function _validate_save($query_options, $old_record, $data, $loc_data, &$validate = NULL){
		$success = TRUE;

		$validate_fields = array();
		if(!empty($this->editable_fields_details) && is_array($this->editable_fields_details)){
			foreach($this->editable_fields_details as $field_name => $field_info){

				if(!empty($field_info['validate'])){
					$validate_fields [] = $field_name;
					$this->form_validation->set_rules($field_name, ('lang:field_'.$field_name), $field_info['validate']);
				}
			}	
		}

		if(empty($validate_fields)) return TRUE;
		
		$this->form_validation->set_data($data);
		$success = $this->form_validation->run() != FALSE;

		foreach($validate_fields as $field_name){
			$validate['data'][ $field_name] = data($field_name, $data);

			$msg = $this->form_validation->error($field_name);
			if(!empty($msg)){
				$validate['fields'][ $field_name] = $msg;
			}
		}

		return $success;
	}
			
	public function editor($record_id=false){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_EDITOR'))){
			return;
		}
		
		$record = NULL;

		$vals = NULL;

		$action = 'add';
		
		if(!empty($record_id)){
			$query_opts = $this->_select_options('editor', array($this->_target_model->pk_field=>$record_id));
			$data = $this->_target_model->read($query_opts);
			$data = (array) $data;
			if(empty($data[$this->_target_model->pk_field])){
				return $this->_show_404('record_not_found');
			}
			
			$vals['id'] = $record_id;
			$vals['record_id'] = $record_id;
			$vals['record'] = (array) $this->_mapping_row($data, 'editor');
			$vals['data'] = $data;
			$action = 'edit';
		}else{
			$vals['id'] = NULL;
			$vals['record_id'] = NULL;
			$vals['record'] = NULL;
			$vals['data'] = (array) $this->_target_model->new_default_values();
		}
		$vals = $this->_get_default_vals($action, $vals);

		if($action == 'add' && !$this->add_enabled){
			return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
		}

		if($action == 'edit' && !$this->edit_enabled){
			return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
		}
		
		// localized content
		$vals['loc'] = array();

		if($this->localized){
			if(isset($vals['record']['loc'])){
				$vals['loc'] = $vals['record']['loc'];
			}
		}

		$view_path = $this->_get_render_view($action);
		
		if($this->uri->is_extension('js')){
			$this->output->set_content_type('text/javascript');
			return $this->_render($view_path.'.js',$vals);
		}
		// if($this->_is_ext('data') && $this->_is_debug()){
		// 	$vals['debug']['queries'] = $this->_target_model->get_db()->queries;
		// 	return $this->_api($vals);
		// }
		if($this->_is_ext('html'))
			return $this->_render($view_path,$vals);

		return $this->_show_404('extension_not_match');
	}

	protected function _after_save_localized($action, $id, $old_record, $data, $loc_data, &$vals = false, $locale = null){
		$editor_info = $this->_get_editor_info();
	}

	protected function _after_save($action, $id, $old_record, $data, $loc_data, &$vals = false){
		$editor_info = $this->_get_editor_info();
	}

	// For batch action
	public function remove($record){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_REMOVE'), FALSE )){
			return ERROR_MISSING_PERMISSION;
		}

		if(!$this->remove_enabled){
			return ERROR_MISSING_PERMISSION_MSG;
		}
		

		if(empty($this->relationship_model)){
			$this->relationship_model = new RelationshipModel();
		}

		$record_id = $record[$this->_target_model->pk_field] ;

		$this->_delete_row($record_id, $record);

		return TRUE;
	}

	// For URL action
	public function delete(){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_REMOVE'))){
			return ;
		}

		if(!$this->remove_enabled){
			return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG);
		}

		if(empty($this->relationship_model)){
			$this->relationship_model = new RelationshipModel();
		}
		
		$ids = $this->input->get_post('ids');
		$ids = explode(",", trim($ids));
		if(!is_array($ids)){
			return $this->_error(ERROR_INVALID_DATA, lang(ERROR_INVALID_DATA_MSG));
		}
		
		$query_opts = $this->_select_options('delete', array($this->_target_model->pk_field=>$ids));

		$records = $this->_target_model->find($query_opts);
		if(is_array($records) && count($records)>0){
			foreach($records as $idx => $record){

				$record_id = $record[$this->_target_model->pk_field];
				
				
				$this->_delete_row($record_id, $record);
			}
			
			
			return $this->_api(array('data'=>$ids));
		}else{
			return $this->_error(ERROR_NO_RECORD_LOADED, lang(ERROR_NO_RECORD_LOADED_MSG));
		}
	}

	protected function _delete_row($record_id, $record = null){
		
		$query_opts = $this->_select_options('delete', array($this->_target_model->pk_field=>$record_id));

		//log_message('debug',get_class($this).'/_delete_row['.__LINE__.'] query='.print_r($query_opts, true));

		if(empty($record)){
			$record = $this->_target_model->read($query_opts);
		}

		$this->_target_model->delete($query_opts);

		// remove all related records
		$this->relationship_model->initialize(array('mode'=>PublishableModel::MODE_STAGING));
		$this->relationship_model->delete(array(
			'ref_table'=>$this->_target_model->table,
			'ref_id'=>$record_id,
		));

		$this->_after_delete($record_id, $record);
		$this->_clear_cache($record);


		if($this->staging_enabled){

			$record2 = $this->_published_model->read($query_opts);

			$this->relationship_model->initialize(array('mode'=>PublishableModel::MODE_PUBLISHED));
			$this->relationship_model->delete(array(
				'ref_table'=>$this->_target_model->table,
				'ref_id'=>$record_id,
			));

			$this->_published_model->delete($query_opts);
			$this->_after_delete($record_id, $record2);

			$this->_clear_cache($record2);

		}
	}

	/**
	 * [publish description]
	 * @param  int/string $id     [description]
	 * @param  boolean $return [description]
	 * @return mixed          [description]
	 */
	
	var $relationship_model;
	public function publish($id=null,$return=false){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_PUBLISH'), !$return )){
			log_message('error','Cannot publish content: Invalid permission or session.');
			if(!$return) 
				return $this->_error(ERROR_INVALID_SESSION, 'Valid session required.');
			return array('error'=>array('code'=>ERROR_INVALID_SESSION, 'message'=>'Valid session required.'));;
		}

		if(empty($this->relationship_model)){
			$this->relationship_model = new RelationshipModel();
		}

		if(!empty($id[ $this->_target_model->pk_field ])){
			$id = $id[ $this->_target_model->pk_field ];
		}
		
		if(empty($id)){
			if(!$return) 
				return $this->_error(ERROR_INVALID_DATA, 'Passed invalid value.');
			return array('error'=>array('code'=>ERROR_INVALID_DATA, 'message'=>'Passed invalid value.'));;
		}

		$published_query = array( $this->_published_model->pk_field=>$id );
		
		$old_rows = $this->_published_model->find( $published_query );
		
		if(is_array($old_rows)){
			foreach($old_rows as $old_row){
				if(isset($old_row[$this->_published_model->pk_field])){

					$this->_before_publish($old_row);
				}
			}
		}

		$result = NULL;

		$editor_info = $this->_get_editor_info();

		// update flag
		$this->_target_model->save(array(
			$this->_target_model->publish_field => PublishableModel::PUBLISH_STATUS_PUBLISHED,
			'publish_date'=>time_to_date(),
			'publish_by'=> $editor_info['_user_type'],
			'publish_by_id'=> $editor_info['_user_id'],
		), array(
			$this->_target_model->pk_field=>$id, 
		));

		if($this->localized){

			// list out all localized records from target model (locale key based records)
			$loc_rows = $this->_target_model->find(array(
				'_field_based'=>'locale',
				$this->_target_model->pk_field=>$id, 
			));

			if(is_array($loc_rows)){
				foreach($loc_rows as $locale => $row){
					$result[ $locale ] = $this->_publish_row($row);
				}
			}
		}else{
			$query_opts = array(
				$this->_target_model->pk_field=>$id, 
			);
			$row = $this->_target_model->read($query_opts);
			$result = $this->_publish_row($row);

		}

		$this->_publish_related($id);

		$this->_after_publish($id);
		
		if($return){
			return compact('result','id');
		}
		
		return $this->_api(compact('result','id'));
	}

	protected function _before_publish($old_row){

		$this->_clear_cache($old_row);


		// select old record before publish
		$published_query = array();

		$field_name = $this->_published_model->pk_field;
		if(isset($old_row[$field_name]))
			$published_query[ $field_name ] = $old_row[$field_name];


		if(!empty($published_query)){
			$this->_published_model->delete( $published_query );

			log_message('debug',  __METHOD__.'@'.__LINE__.', delete row :'.$this->_published_model->get_db()->last_query());
		}
	}

	protected function _publish_row ( $new_row ){

		$editor_info = $this->_get_editor_info();
		
		$new_row[ $this->_published_model->publish_field ] = PublishableModel::PUBLISH_STATUS_PUBLISHED;
		$new_row [ 'publish_date'] = time_to_date();
		$new_row [ 'publish_by']= $editor_info['_user_type'];
		$new_row [ 'publish_by_id']= $editor_info['_user_id'];
		$result = $this->_published_model->save($new_row);

		if(!empty($result['id'])){
			return TRUE;
		}
		log_message('error', __METHOD__.'@'.__LINE__.', cannot save new record for '.print_r(array('new_row'=>$new_row), true));
		return FALSE;
	}

	protected function _publish_related ( $id ){

		$editor_info = $this->_get_editor_info();

		// initialize table
		$this->relationship_model->initialize(array('mode'=>PublishableModel::MODE_STAGING));

		// get all related records
		$relationships = $this->relationship_model->find(array('ref_table'=> $this->_target_model->table, 'ref_id'=> $id));

		// iniitlaize table for production
		$this->relationship_model->initialize(array('mode'=>PublishableModel::MODE_PUBLISHED));

		// remove all old records
		$this->relationship_model->delete(array('ref_table'=> $this->_target_model->table, 'ref_id'=> $id));

		// if exist, insert new-set data
		if(!empty($relationships) && is_array($relationships)){
			foreach($relationships as $relationship_row){
				$relationship_row [ $this->relationship_model -> publish_field ] = PublishableModel::PUBLISH_STATUS_PUBLISHED;
				$relationship_row [ 'publish_date'] = time_to_date();
				$relationship_row [ 'publish_by']= $editor_info['_user_type'];
				$relationship_row [ 'publish_by_id']= $editor_info['_user_id'];
				$this->relationship_model->save($relationship_row);
			}
		}

	}



	public function clone_record($record_id=false, $return = false){
		if( $this->_restrict($this->_get_permission_scopes('RECORD_CLONE'), !$return )){
			log_message('error','Cannot publish content: Invalid permission or session.');
			if(!$return) 
				return $this->_error(ERROR_INVALID_SESSION, 'Valid session required.');
			return array('error'=>array('code'=>ERROR_INVALID_SESSION, 'message'=>'Valid session required.'));;
		}

		if(empty($this->relationship_model)){
			$this->relationship_model = new RelationshipModel();
		}

		if(is_array($record_id) && isset($record_id[ $this->_target_model->pk_field ])){
			$record_id = $record_id[ $this->_target_model->pk_field ];
		}
		
		if(empty($record_id)){
			if(!$return) 
				return $this->_error(ERROR_INVALID_DATA, 'Passed invalid value.');
			return array('error'=>array('code'=>ERROR_INVALID_DATA, 'message'=>'Passed invalid value.'));;
		}

		$query_opts = array(
			$this->_target_model->pk_field=>$record_id, 
		);
		$record = $new_record = $this->_target_model->read($query_opts);

		$result = NULL;

		$editor_info = $this->_get_editor_info();


		// get an new id
		$new_record_id = NULL;

		if($this->_target_model->use_guid){
			$new_record_id = guid();
		}elseif($this->_target_model->use_global_id){
			$new_record_id = UniqueIDHelper::shared()->get_id ( $this->_target_model->table);
		}else{
			$new_record_id = NULL;
		}

		if($this->localized){

			// list out all localized records from target model (locale key based records)
			$loc_rows = $this->_target_model->find(array(
				'_field_based'=>'locale',
				$this->_target_model->pk_field=>$record_id, 
			));

			if(is_array($loc_rows)){
				foreach($loc_rows as $locale => $_record){

					$_result = $this->_clone_row($_record, $new_record_id);
					$result[ $locale ] = $_result;

					// if no new_id found
					if(empty($new_record_id ) && isset($_result [ $this->_target_model->pk_field ])){
						$new_record_id = $_result [ $this->_target_model->pk_field ];

					}
					// get the latest record
					$query_opts = array(
						$this->_target_model->pk_field=>$new_record_id, 
					);
					$_new_record = $this->_target_model->read($query_opts);

					$this->_after_save_localized('clone', $new_record_id, $_record, $_new_record, NULL, $vals, $locale);
				}
			}
			$this->_after_save('clone', $new_record_id, null, null, null, $vals);
		}else{
			$result = $this->_clone_row($record, $new_record_id);

			if(empty($new_record_id ) && isset($result [ $this->_target_model->pk_field ])){
				$new_record_id = $result [ $this->_target_model->pk_field ];
			}
			// get the latest record
			$query_opts = array(
				$this->_target_model->pk_field=>$new_record_id, 
			);
			$new_record = $this->_target_model->read($query_opts);

			$this->_after_save('clone', $new_record_id, $record, $new_record, NULL, $vals);
		}



		$this->_clone_related($record_id, $new_record_id);


		if($this->_is_ext('data')){
			return $this->_api(array('id'=>$save_result['id']));
		}
		return redirect($this->endpoint_path_prefix.'/'.$new_record_id.'/edit');
	}

	protected function _clone_row ( $new_row , $new_id = null){

		$new_row[ $this->_target_model->publish_field ] = PublishableModel::PUBLISH_STATUS_UNPUBLISHED;
		unset($new_row[ 'publish_date' ]);
		unset($new_row[ 'publish_by' ]);
		unset($new_row[ 'publish_by_id' ]);

		if(!empty($new_id))
			$new_row[ $this->_target_model->pk_field ] = $new_id;

		$record = null;
		$action = 'clone';

		$locale = isset($new_row['locale']) ? $new_row['locale'] : null;
		$success = $this->_before_save($action, $record, $new_row, $loc_data, $locale);

		$result = $this->_target_model->save($new_row);

		return $result;
	}

	protected function _clone_related ( $old_id, $new_id ){

		$editor_info = $this->_get_editor_info();

		// initialize table
		$this->relationship_model->initialize(array('mode'=>PublishableModel::MODE_STAGING));

		// get all related records
		$relationships = $this->relationship_model->find(array('ref_table'=> $this->_target_model->table, 'ref_id'=> $old_id));

		if(!empty($relationships)){
			foreach($relationships as $relationship_row){

				$relationship_row[ $this->_target_model->publish_field ] = PublishableModel::PUBLISH_STATUS_UNPUBLISHED;
				unset($relationship_row[ 'publish_date' ]);
				unset($relationship_row[ 'publish_by' ]);
				unset($relationship_row[ 'publish_by_id' ]);
				unset($relationship_row[ $this->_target_model->pk_field ]);
				$relationship_row['ref_id'] = $new_id;

				$this->relationship_model->save($relationship_row);

			}
		}
	}


	protected function _after_clone($record, $record_id, $new_record, $new_record_id, &$vals = NULL){

	}

	protected function _mapping_row($raw_row, $action = 'default'){

		$row = $this->_mapping_row2($raw_row, $action);

		if($this->localized && ($action == 'editor' || $action == 'view')){
			$row['loc'] = array();

			$locale_keys = $this->lang->get_available_locale_keys();

			$_localized_rows = $this->_target_model->find(array( 
				$this->_target_model->pk_field => $raw_row[ $this->_target_model->pk_field ], 
				'_field_based'=>'locale',
				'locale'=>$locale_keys,
			));
			foreach($_localized_rows as $locale_key => $_localized_row){
				if($action == 'editor'){
					$row['loc'][$locale_key] = (array) $_localized_row;
				}else{
					$row['loc'][$locale_key] = $this->_mapping_row2($_localized_row, $action);
				}
			}
		}

		return $row;
	}

	protected function _mapping_row2($raw_row, $action){

		$row = array();
		if(empty($raw_row['id'])) return NULL;

		if(!empty($this->mapping_fields) && is_array($this->mapping_fields)){
			foreach($this->mapping_fields as $field_name){
				$row[$field_name] = data($field_name, $raw_row);
			}
		}

		if(isset($raw_row['id'])){
			$row['id'] = $raw_row['id'];
		}

		if(isset($raw_row['_mapping'])){
			$row['_mapping'] = $raw_row['_mapping'];
		}

		if(isset($raw_row['slug'])){
			$row['slug'] = $raw_row['slug'];
		}

		if(isset($raw_row[$this->priority_field])){
			$row[$this->priority_field] = intval($raw_row[$this->priority_field]);
		}


		if(is_array($this->_target_model->fields_details)){
			foreach($this->_target_model->fields_details as $field_name => $field_info){
				if(!empty($raw_row[$field_name]) && (($action =='editor' ) || (isset($field_info['listing']) && $field_info['listing']) || (isset($field_info['export']) && $field_info['export']))){
					$row[ $field_name ] = $raw_row[$field_name];

					// if(isset($field_info['control']) && $field_info['control'] == 'select' && isset($field_info['control_type']) && $field_info['control_type'] == 'file' && isset($field_info['is_image']) && $field_info['is_image']){

					// 	if(!empty($raw_row[$field_name]))
					// 		$row[$field_name.'_image'] = $this->resource->picture_mapping( $raw_row[$field_name],'file','thumbnail' );
					// }
				}
			}
		}

		return $row;
	}
}