<?php 

namespace Dynamotor\Extended\Helpers;


use \Dynamotor\Extended\Models\UniqueIDModel;

class UniqueIDHelper
{
	static $instance ;

	var $model;

	static function shared(){
		if(UniqueIDHelper::$instance == NULL){
			UniqueIDHelper::$instance = new UniqueIDHelper();
		}
		return UniqueIDHelper::$instance;
	}

	public function __construct(){
		$this->model = new UniqueIDModel;
	}

	/**
	 * Create an new id or retreive an exist id for query
	 * @param  string $ref_table      [description]
	 * @param  mixed $ref_id         [description]
	 * @param  [type] $default_locale [description]
	 * @param  [type] $sub_table      [description]
	 * @param  [type] $sub_id         [description]
	 * @return [type]                 [description]
	 */
	public function get_id($ref_table, $ref_id = null, $default_locale = null, $sub_table= null, $sub_id = null){
		$query_opts = compact('ref_table','ref_id');

		$result = NULL;

		// ref_id is required for 
		if(!empty($query_opts['ref_id']))
			$result = $this->model->read($query_opts);
		if(isset($result['id'])){
			return $result['id'];
		}else{
			$save_result = $this->model->save(compact('ref_table','ref_id','default_locale','sub_table','sub_id'));

			return $save_result['id'];
		}
	}
}