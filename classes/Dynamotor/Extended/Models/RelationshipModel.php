<?php 

namespace Dynamotor\Extended\Models;

use \Dynamotor\Extended\Models\PublishableModel;

class RelationshipModel extends PublishableModel
{
	public $base_table = 'relationships2';

	var $table_indexes = array(
		array('status','publish_status'),
	);

	var $use_guid = true;

	var $fields_details =array(

		'id'=>array(
			'type'=>'VARCHAR',
			'constraint' => 36,
			'pk'=>TRUE,

			'listing'=>TRUE,
			'listing_hidden'=>TRUE,
		),

		'publish_status'=> array(
			'localized'=>TRUE,
			'type'=>'INT',
			'constraint' => 2,
			'default'=>'0',

			'editable'=>FALSE,

			'pk'=>TRUE,
		),
		'ref_table' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null'         => TRUE,
		),
		'ref_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null'         => TRUE
		),

		'term_table' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null'         => TRUE,
		),
		'term_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null'         => TRUE,
		),
		'term_type'=>array(
			'type'       => 'VARCHAR',
			'constraint' => 50,
			'null'         => TRUE,
		),
		'sequence'=>array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'default'         => '0',
		),

		'parameters'=>array(
			'type'       => 'TEXT',
			'null'       => true,
		),

		'publish_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,

			'listing'=>TRUE,
		),
		'publish_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'publish_by_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
		),

		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null' => TRUE,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null' => TRUE,
		),

	);
}