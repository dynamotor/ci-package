<?php 

namespace Dynamotor\Extended\Models;

use \Dynamotor\Core\HC_Model;

class UniqueIDModel extends HC_Model
{
	var $table = 'uniqueid';

	var $table_indexes = array(
		array('ref_table','ref_id'),
		array('default_locale'),
	);

	var $fields_details =array(

		'id' => array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'pk'             => TRUE,
			'auto_increment' => TRUE
		),
		'ref_table' => array(
			'type'           => 'VARCHAR',
			'constraint'     => 60,
		),
		'ref_id' => array(
			'type'           => 'VARCHAR',
			'constraint'     => 60,
			'null'             => TRUE,
			'default'=>NULL,
		),
		
		'default_locale' => array(
			'type'       => 'VARCHAR',
			'constraint' => '5',
		),
		'slug' => array(
			'type'       => 'VARCHAR',
			'constraint' => '200',
			'null'       => TRUE,
			'validate'=>'trim|required',
		),

		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null' => TRUE,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'null' => TRUE,
		),
	);
}