<?php 

namespace Dynamotor\Extended\Models;

use \Dynamotor\Core\HC_Model;
use \Dynamotor\Extended\Helpers\UniqueIDHelper;

class PublishableModel extends HC_Model
{
	const MODE_STAGING = 0;
	const MODE_PUBLISHED = 1;

	const PUBLISH_STATUS_UNPUBLISHED = 0;
	const PUBLISH_STATUS_PUBLISHED = 1;
	const PUBLISH_STATUS_PUBLISHED_WITH_MODIFIED = 2;

	protected $_mode = self::MODE_STAGING;

	/**
	 * Base table name, required for switching mode
	 * @var string
	 */
	public $base_table = 'tables'; 

	/**
	 * Table name prefix of published data
	 * @var string
	 */
	public $published_table_prefix = 'pbd_';

	/**
	 * Table name prefix of staging data
	 * @var string
	 */
	public $staging_table_prefix = '';



	public $publish_field = 'publish_status';

	public function initialize($config = NULL){

		if(is_array($config )){
			foreach($config as $key => $val){
				if(isset($this->$key)){
					$this->$key = $val;
				}
				$protected_key = '_'.$key;
				if(isset($this->$protected_key)){
					$this->$protected_key = $val;
				}
			}
		}

		$this->load_table_name();

		log_message('debug','PublishableModel['.get_class($this).','.$this->_mode.','.$this->table.'] initialized. config='.print_r($config,true));

		parent::initialize($config);
	}

	
	public function base_table(){
		return $this->base_table;
	}

	/**
	 * [set_mode description]
	 * @param int $value [description]
	 */
	public function set_mode($value ){
		if($value != $this->_mode){
			if($this->on_change_to_mode($value, $this->_mode)){
				$this->_mode = $value;

				$this->initialize();
			}
		}
		return $this;
	}

	/**
	 * Current mode of this instance
	 * @return int [description]
	 */
	public function get_mode(){
		return $this->_mode;
	}


	/**
	 * [change_to_mode description]
	 * @param  int $new_mode [description]
	 * @param  int $old_mode [description]
	 * @return boolean           [description]
	 */
	protected function on_change_to_mode($new_mode, $old_mode){

		return TRUE;
	}

	/**
	 * Table name will be re-assigned after change mode
	 */
	protected function load_table_name(){

		if($this->get_mode() == self::MODE_PUBLISHED){
			$this->table = $this->published_table_prefix. $this->base_table;
		}elseif($this->get_mode() == self::MODE_STAGING){
			$this->table = $this->staging_table_prefix. $this->base_table;
		}
	}

	protected function assign_insert_record_id( &$data, &$sql_data){

		$info = array();

		// If this model use_global_id is true, we create id from global table
		if(isset($this->use_global_id) && $this->use_global_id){

			if(empty($sql_data[ $this->pk_field ])){
				$info['id'] = UniqueIDHelper::shared()->get_id( $this->base_table );
				$sql_data[ $this->pk_field ] = $info['id'];
			}else{
				$info['id'] = $sql_data[ $this->pk_field ];
			}
			$info['is_assigned_id'] = true;
		}else{

			$info = parent::assign_insert_record_id($data, $sql_data);
		}

		return $info;
	}
}