<?php

namespace Dynamotor\Data;

class DataArrayObject extends \ArrayObject { 

    private $___class = null;
    
    public function __get($key)
    {
        return $this[$key];
    }

    public function __set($key, $value)
    {
        $this[$key] =  $value;
    }
    
    public function __call($key, $args)
    {
        log_message('debug', __METHOD__.':'.$key);

        if(is_object($this->___class) && is_callable([$this->___class, $key])){
            return call_user_func_array([$this->___class, $key],$args);
        }
        return is_callable($c = $this->__get($key)) ? call_user_func_array($c, $args) : null;
    }

    public function toArray()
    {
        return $this->export();
    }

    public function getArrayCopy()
    {
        return parent::getArrayCopy();
    }

    public function importObj($class,  $array = []){
        $this->___class = $class;
        if(count($array) > 0){
            $this->import($array);
        }
        return $this;
    }

    public function import($input)
    {
        $this->exchangeArray($input);
        return $this;
    }

    public function export()
    {
        return $this->objectToArray($this->getArrayCopy());
    }

    public function objectToArray ($object) {
        $o = [];
        foreach ($object as $key => $value) {
           $o[$key] = is_object($value) ? (array) $value: $value;
        }
        return $o;
    }

} 

