<?php 

namespace Dynamotor\Data;

use \Dynamotor\Core\HC_Model;

class FetchedResult extends DataArrayObject
{

    protected $_model = null;
    public function setModel(HC_Model $model)
    {
        $this->_model = $model;
    }

    public function model()
    {
        return $this->_model;
    }

    protected static $properties = ['data', 'total_record', 'total_page', 'page', 'index_from', 'index_to', 'offset', 'limit'];

    /**
     * Setter function for internal properties
     *
     * @param [type] $name
     * @param [type] $value
     */
    public function __set($name, $value)
    {
        if(in_array($name, self::$properties )){
            return $this[$name] = $value;
        }
        return parent::__set($name, $value);
    }

    /**
     * Getting function for internal properties
     *
     * @param [type] $name
     */
    public function __get($name)
    {
        if(in_array($name, self::$properties )){
            return $this[$name];
        }
        return parent::__get($name);
    }
}