<?php 

namespace Dynamotor\Data;


use \Dynamotor\Core\HC_Model;

class Entity extends DataArrayObject
{
    

    protected $_model = null;
    public function setModel(HC_Model $model)
    {
        $this->_model = $model;
    }

    public function model()
    {
        return $this->_model;
    }
}