<?php 

namespace Dynamotor\Modules;

use \Dynamotor\Core\HC_Module; 
use \Dynamotor\Modules\Auth\SimpleAuth;
use \Dynamotor\Modules\Auth\Acl;

class AdminModule extends HC_Module
{
	public $email_layout = 'layouts/admin_email';

	public $config = NULL;

	public $auth;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin_account_model');
		$this->load->model('admin_account_permission_model');
		$this->load->model('admin_account_role_model');
		$this->load->model('admin_role_model');
		$this->load->model('admin_role_permission_model');
		$this->load->model('admin_permission_model');

	}

	public function initialize($config = NULL){

		$this->config = $config;

		if(!isset($config['auth_config'])) $config['auth_config'] = array();
		if(!isset($config['acl_config'])) $config['acl_config'] = array();

		$config['auth_config']['db'] = $this->admin_account_model->get_db();
		// Feature: Authentication
		$this->auth = new SimpleAuth($config['auth_config']);

		// Feature: Access Control List
		$this->acl = new Acl($config['acl_config']);
	}

	public function get_account($conditions)
	{
		if(isset($conditions)){
			$conditions = ['id'=> $conditions];
		}

		return $this->admin_account_model->read($conditions);
	}


	public function change_password($user, $new_pass=NULL, $email_notify=FALSE, $email_subject = 'Password has been changed' ){


		log_message('debug','Admin/change_password: params= '.print_r(compact('user','new_pass','email_notify'),true));

		if(is_string($user)){
			$user = $this->admin_account_model->read(array('id'=>$user));
		}
		if(empty($user['id'])){
			log_message('error','AdminModule/change_password: Member account not found.');
			return $this->error(ERROR_INVALID_DATA, 'User account not found');
		}

		if(empty($new_pass)){
			$this->load->helper('string');
			$new_pass = random_string('alnum',16);
		}

		log_message('debug','AdminModule/change_password: params= '.print_r(compact('user','new_pass','email_notify'),true));

		$new_data = array(
			'login_pass' => $this->encode($new_pass),
		);

		$result = $this->admin_account_model->save($new_data,array('id'=>$user['id']));
		if(empty($result['id'])){
			log_message('error','AdminModule/change_password: cannot save new password.');
			return FALSE;
		}else{
			log_message('debug','AdminModule/change_password: saved new password: '.$this->db->last_query());
		}


		if($email_notify){
			$this->notify_email('email/change_password', $user['email'],  $email_subject, array(
				'name'=>$user['name'],
				'new_pass'=>$new_pass,
			));
		}
		return TRUE;
	}


	public function notify_email($view_setting, $to_email, $subject = 'System Notice', $vals=false){

		if(!class_exists('Swift_Message')){
			log_message('error','AdminModule/notify_email: Swift_Message does not exist.');
			return $this->error(-1, 'Required class does not exist.');
		}
		
		$layout = $this->email_layout ;
		$view = $view_setting;
		if(is_array($view_setting)){

			if(!empty($view_setting['layout'])){
				$layout = $view_setting['layout'];
			}
			if(isset($view_setting['view'])){
				$view = $view_setting['view'];
			}
		}

		$vals['view'] = $view;

		$from_addr = $this->config->item('sender_from');
		$from_name = $this->config->item('sender_from_name');

		if(empty($from_addr) && !empty($_SERVER['HTTP_HOST'])){
			$from_addr = 'no-reply@'.$_SERVER['HTTP_HOST'];
		}
		if(empty($from_name) && !empty($_SERVER['HTTP_HOST'])){
			$from_name = $_SERVER['HTTP_HOST'];
		}

		$this->load->config('email');

		$this->load->library('email');

		$this->email->initialize();

		$content = $this->load->view($layout,$vals, TRUE);

		$this->email->subject($subject);
		$this->email->from($from_addr, $from_name);
		$this->email->to($to_email);
		$this->email->message($content);
		
		
		try{
			$result = $this->email->send();
		}catch(Exception $exp){
			log_message('error','AdminModule/notify_email, send email failure: '.$exp->getMessage().', config='.print_r(compact('protocol','smtp_host','smtp_user','smtp_pass'),true));
			return FALSE;	
		}

		if($result){
			return TRUE;
		}else{
			log_message('error','AdminModule/notify_email, send email failure: '.print_r($failures,true));
			return FALSE;
		}
	}

	public function encode($val){
		$this->load->library('encrypt');
		// getting encryption key
		$encryption_key = $this->config->item($this->config['auth_config']['encryption_key']);
		if(empty($encryption_key)){
			$encryption_key = $this->config->item('encryption_key');
		}

		return $this->encrypt->encode($val, $encryption_key);
	}

	public function decode($val){
		$this->load->library('encrypt');
		// getting encryption key
		$encryption_key = $this->config->item($this->config['auth_config']['encryption_key']);
		if(empty($encryption_key)){
			$encryption_key = $this->config->item('encryption_key');
		}

		return $this->encrypt->decode($val, $encryption_key);
	}
}